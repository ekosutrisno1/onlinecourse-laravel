<?php

namespace App\Exports;

use App\Ujian;
use Maatwebsite\Excel\Concerns\FromCollection;

class UjianExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Ujian::all();
    }
}
