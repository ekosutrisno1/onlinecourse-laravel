<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JadwalUser extends Model
{
    protected $table = 'jadwal_users';
    protected $fillable =['registered_user_id', 'jadwal_id' ];
}
