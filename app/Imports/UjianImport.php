<?php

namespace App\Imports;

use App\Ujian;
use Maatwebsite\Excel\Concerns\ToModel;

class UjianImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Ujian([
           'materi_id' => $row[1],
           'tanggal' => $row[2],
           'pertanyaan' => $row[3],
           'jawaban' => $row[4],
           'status' => $row[5],
        ]);
    }
}
