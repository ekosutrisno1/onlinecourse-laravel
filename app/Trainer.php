<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trainer extends Model
{
    protected $fillable = ['name', 'email', 'user_id', 'status', 'rating', 'alamat', 'no_hp'];
}
