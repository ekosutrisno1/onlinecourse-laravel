<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Materi extends Model
{
    public function ujian()
    {
        return $this->hasMany('App\Ujian');
    }
}
