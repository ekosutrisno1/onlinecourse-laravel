<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ujian extends Model
{
    protected $table ='ujians';
    protected $fillable =['materi_id','tanggal', 'pertanyaan','jawaban', 'status'];


    public function materis()
    {
        return $this->belongsTo('App\Materi');
    }
}
