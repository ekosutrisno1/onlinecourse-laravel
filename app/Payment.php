<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table ='payments';
    protected $fillable =['registerde_user_id', 'status'];
}
