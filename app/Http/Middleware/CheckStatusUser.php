<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class CheckStatusUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userRole = Auth::user()->role;
        if ($userRole === 2) {
            return redirect('/unauthorize');
        }

        return $next($request);
    }
}
