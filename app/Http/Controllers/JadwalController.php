<?php

namespace App\Http\Controllers;

use App\Jadwal;
use App\Trainer;
use DB;
use Illuminate\Http\Request;

class JadwalController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jadwal = DB::table('jadwals')
            ->join('trainers', 'jadwals.trainer_id', '=', 'trainers.id')
            ->select('jadwals.*', 'trainers.name')
            ->get();

        return view('jadwal.index', ['jadwal' => $jadwal]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $trainer = Trainer::where('status', '=', 1)->get();
        return view('jadwal.add', ['trainer' => $trainer]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $jadwal = new Jadwal;
        $jadwal->kode_jadwal = $request->kode_jadwal;
        $jadwal->start_date = $request->start_date;
        $jadwal->end_date = $request->end_date;
        $jadwal->trainer_id = $request->trainer_id;

        $jadwal->save();

        return redirect('/jadwal')->with('success', 'Jadwal telah ditambahkan.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Jadwal  $jadwal
     * @return \Illuminate\Http\Response
     */
    public function show(Jadwal $jadwal)
    {
        $jadwalJoin = DB::table('jadwals')
            ->join('trainers', 'jadwals.trainer_id', '=', 'trainers.id')
            ->where('jadwals.id', '=', $jadwal->id)
            ->select('jadwals.*', 'trainers.name')
            ->get();

        $dataMateri = DB::table('materis')
            ->join('jadwals', 'materis.jadwal_id', '=', 'jadwals.id')
            ->join('trainers', 'jadwals.trainer_id', '=', 'trainers.id')
            ->where('jadwals.id', '=', $jadwal->id)
            ->select('materis.*', 'trainers.name')
            ->get();

        foreach ($jadwalJoin as $key => $dataJadwal) {}

        return view('jadwal.show', ['jadwal' => $dataJadwal, 'materi' => $dataMateri]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Jadwal  $jadwal
     * @return \Illuminate\Http\Response
     */
    public function edit(Jadwal $jadwal)
    {
        $editData = Jadwal::find($jadwal->id);
        $trainer = Trainer::where('status', '=', 1)->get();

        return view('jadwal.edit', ['jadwal' => $editData, 'trainer' => $trainer]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Jadwal  $jadwal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Jadwal $jadwal)
    {
        $data = Jadwal::find($jadwal->id);
        $data->kode_jadwal = $request->kode_jadwal;
        $data->trainer_id = $request->trainer_id;
        $data->start_date = $request->start_date;
        $data->end_date = $request->end_date;

        $data->save();

        return redirect('/jadwal')->with('success', 'Jadwal ' . $jadwal->kode_jadwal . ' telah diupdate.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Jadwal  $jadwal
     * @return \Illuminate\Http\Response
     */
    public function destroy(Jadwal $jadwal)
    {
        $dataDelete = Jadwal::find($jadwal->id);
        $dataDelete->delete();

        return redirect('/jadwal')->with('success', 'Jadwal ' . $jadwal->kode_jadwal . ' Berhasil Dihapus!');
    }
}
