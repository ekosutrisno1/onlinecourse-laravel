<?php

namespace App\Http\Controllers;

use App\Jadwal;
use App\Materi;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class MateriController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $email = auth()->user()->email;
        $role = auth()->user()->role;

        if ($role != 2) {
            $materi = Materi::all();
            return view('materi.index', ['materi' => $materi]);
        } else {
            return view('errorpage.error_custom')->with('message', 'Hai ' . $email . ' Kamu tidak mendapat izin mengakses halaman ini.');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $jadwal = Jadwal::all();
        return view('materi.add', ['jadwal' => $jadwal]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'judul' => 'required',
            'jadwal_id' => 'required',
            'status' => 'required',
            'attachment' => 'image|nullable|max:1999',
        ]);

        //handle file Uploda
        if ($request->hasFile('attachment')) {
            $file = $request->file('attachment');
            $fileNameExtension = $file->getClientOriginalExtension();
            $fileNameToStore = 'attach_' . date('YmdHis') . '.' . $fileNameExtension;

            $path = $request->file('attachment')->storeAs('public/attachment', $fileNameToStore);
        } else {
            $fileNameToStore = 'bg03.svg';
        }

        // save Materi
        $materi = new Materi;
        $materi->judul = $request->judul;
        $materi->jadwal_id = $request->jadwal_id;
        $materi->tanggal = $request->tanggal;
        $materi->attachment = $fileNameToStore;
        $materi->status = $request->status;
        $materi->sinopsis = $request->sinopsis;

        $materi->save();

        return redirect('/materi')->with('success', 'Materi telah ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Materi  $materi
     * @return \Illuminate\Http\Response
     */
    public function show(Materi $materi)
    {
        $materi = DB::table('materis')
            ->join('jadwals', 'materis.jadwal_id', '=', 'jadwals.id')
            ->join('trainers', 'jadwals.trainer_id', '=', 'trainers.id')
            ->select('materis.*', 'jadwals.kode_jadwal', 'trainers.name')
            ->where('materis.id', '=', $materi->id)
            ->get();

        foreach ($materi as $key => $mat) {
            $data = $mat;
        }

        return view('materi.show', ['materi' => $data]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Materi  $materi
     * @return \Illuminate\Http\Response
     */
    public function edit(Materi $materi)
    {
        $jadwal = Jadwal::all();
        $data = Materi::find($materi->id);
        return view('materi.edit', ['materi' => $data, 'jadwal' => $jadwal]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Materi  $materi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Materi $materi)
    {
        $this->validate($request, [
            'judul' => 'required',
            'jadwal_id' => 'required',
            'status' => 'required',
            'attachment' => 'image|nullable|max:1999',
        ]);

        //handle file Uploda
        if ($request->hasFile('attachment')) {
            $file = $request->file('attachment');
            $fileNameExtension = $file->getClientOriginalExtension();
            $fileNameToStore = 'attach_' . date('YmdHis') . '.' . $fileNameExtension;

            $path = $request->file('attachment')->storeAs('public/attachment', $fileNameToStore);
        }

        // save Materi
        $materi = Materi::find($materi->id);
        $materi->judul = $request->judul;
        $materi->jadwal_id = $request->jadwal_id;
        $materi->tanggal = $request->tanggal;
        $materi->status = $request->status;
        $materi->sinopsis = $request->sinopsis;
        if ($request->hasFile('attachment')) {
            $materi->attachment = $fileNameToStore;
        }

        $materi->save();

        return redirect('/materi')->with('success', 'Materi telah diperbarui.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Materi  $materi
     * @return \Illuminate\Http\Response
     */
    public function destroy(Materi $materi)
    {
        $data = Materi::find($materi->id);
        if ($data->attachment != 'bg03.svg') {
            //delete attachment
            Storage::delete('public/attachment/' . $data->attachment);
        }

        $data->delete();
        return redirect('/materi')->with('success', 'Materi ' . $materi->judul . ' Berhasil Dihapus!');
    }
}
