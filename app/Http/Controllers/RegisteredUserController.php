<?php

namespace App\Http\Controllers;

use App\JadwalUser;
use App\Payment;
use App\RegisteredUser;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class RegisteredUserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $email = auth()->user()->email;
        $role = auth()->user()->role;

        if ($role != 2) {
            $data = RegisteredUser::orderby('created_at', 'desc')->paginate(4);
            return view('registered_user.index', ['data' => $data]);
        } else {
            return view('errorpage.error_custom')->with('message', 'Hai ' . $email . ' Kamu tidak mendapat izin mengakses halaman ini.');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('registered_user.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $email = auth()->user()->email;
        $nomorIdentitas = 'oc-exoapp-' . Str::random(3) . auth()->user()->id;
        $status = 1;

        $registeredUser = new RegisteredUser;

        $registeredUser->email = $email;
        $registeredUser->status = $status;
        $registeredUser->no_identitas = $nomorIdentitas;
        $registeredUser->alamat = $request->alamat;
        $registeredUser->keterangan = $request->keterangan;
        $registeredUser->gender = $request->gender;
        $registeredUser->tempat_lahir = $request->tempat_lahir;
        $registeredUser->tanggal_lahir = $request->tanggal_lahir;
        $registeredUser->save();

        $regUser = RegisteredUser::where('no_identitas', '=', $nomorIdentitas)->get();

        foreach ($regUser as $key => $value) {
            $data = $value;
        }

        JadwalUser::create([
            'registered_user_id' => $data->id,
            'jadwal_id' => $request->jadwal_id,
        ]);

        Payment::create([
            'registerde_user_id' => $data->id,
            'status' => $status,
        ]);

        return \redirect('/dashboard')->with('status', 'Jadwal telah terambil, lakukan langkah pembayaran untuk dapat akses Materi Jadwal.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RegisteredUser  $registeredUser
     * @return \Illuminate\Http\Response
     */
    public function show(RegisteredUser $registeredUser)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RegisteredUser  $registeredUser
     * @return \Illuminate\Http\Response
     */
    public function edit(RegisteredUser $registeredUser)
    {
        $user = RegisteredUser::find($registeredUser->id);
        return view('registered_user.edit', ['userEdit' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RegisteredUser  $registeredUser
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RegisteredUser $registeredUser)
    {
        $reUser = RegisteredUser::find($registeredUser->id);

        $reUser->status = $request->status;
        $reUser->alamat = $request->alamat;
        $reUser->keterangan = $request->keterangan;
        $reUser->gender = $request->gender;
        $reUser->tempat_lahir = $request->tempat_lahir;
        $reUser->tanggal_lahir = $request->tanggal_lahir;
        $reUser->save();

        return back()->with('success', 'Peserta telah diupdated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RegisteredUser  $registeredUser
     * @return \Illuminate\Http\Response
     */
    public function destroy(RegisteredUser $registeredUser)
    {
        $reUser = RegisteredUser::find($registeredUser->id);
        $jadwalUser = JadwalUser::where('registered_user_id', '=', $reUser->id);

        $reUser->delete();
        $jadwalUser->delete();

        return back()->with('success', 'Peserta telah dihapus.');
    }
}
