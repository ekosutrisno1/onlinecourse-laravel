<?php

namespace App\Http\Controllers;

use App\JawabanUser;
use App\Ujian;
use DB;
use Illuminate\Http\Request;

class JawabanUserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jawabanSoal = DB::table('jawaban_users')
            ->join('materis', 'jawaban_users.materi_id', '=', 'materis.id')
            ->join('users', 'jawaban_users.peserta_id', '=', 'users.id')
            ->select('jawaban_users.*', 'materis.judul', 'users.name')
            ->orderBy('created_at', 'ASC')->paginate(3);

        $email = auth()->user()->email;
        $role = auth()->user()->role;

        if ($role != 2) {
            return view('jawaban.index', ['jawaban' => $jawabanSoal]);
        } else {
            return view('errorpage.error_custom')->with('message', 'Hai ' . $email . ' Kamu tidak mendapat izin mengakses halaman ini.');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $idSoal = $request->id_soal;
        $ujian = Ujian::find($idSoal);

        $jawabanSoal = $ujian->jawaban;

        if ($jawabanSoal == $request->jawaban) {
            $score = 1;
        } else {
            $score = 0;
        }

        $jawabanUser = new JawabanUser;

        $jawabanUser->materi_id = $request->materi_id;
        $jawabanUser->jawaban = $request->jawaban;
        $jawabanUser->status_soal = $request->status_soal;
        $jawabanUser->nomor_soal = $request->nomor_soal;
        $jawabanUser->peserta_id = $request->peserta_id;
        $jawabanUser->score = $score;

        $jawabanUser->save();

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\JawabanUser  $jawabanUser
     * @return \Illuminate\Http\Response
     */
    public function show(JawabanUser $jawabanUser)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\JawabanUser  $jawabanUser
     * @return \Illuminate\Http\Response
     */
    public function edit(JawabanUser $jawabanUser)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\JawabanUser  $jawabanUser
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, JawabanUser $jawabanUser)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\JawabanUser  $jawabanUser
     * @return \Illuminate\Http\Response
     */
    public function destroy(JawabanUser $jawabanUser)
    {
        $jawaban = JawabanUser::find($jawabanUser->id);
        $jawaban->delete();

        return back()->with('success', 'Jawaban telah Berhasil Dihapus!');
    }
}
