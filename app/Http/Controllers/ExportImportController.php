<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Imports\UjianImport;
use App\Exports\UjianExport;
use App\Ujian;
use Maatwebsite\Excel\Facades\Excel;

class ExportImportController extends Controller
{
    public function exportPdf()
    {
        $ujian = Ujian::all();
        $pdf = PDF::loadview('ujian.ujian_pdf', ['ujian' => $ujian]);

        return $pdf->download('ujian.pdf');
    }

    public function getImport()
    {
        return view('ujian.import');
    }

    public function importFile(Request $request)
    {
        $this->validate($request, [
            'file'=>'required|mimes:csv,xls,xlsx'
        ]);

        $file = $request->file('file');
        $nama_file = $file->getClientOriginalName();
        $file->move('doc/', $nama_file);

        Excel::import(new UjianImport, public_path('doc/'.$nama_file));
        $request->session()->flash('success', "Data Berhasil Di Import");

        return redirect('/ujian');
    }

    public function exportFile()
    {
        return Excel::download(new UjianExport, 'ujian.xlsx');
    }
}
