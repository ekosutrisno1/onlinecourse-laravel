<?php

namespace App\Http\Controllers;

use App\Materi;
use App\Payment;
use App\RegisteredUser;
use App\Ujian;
use DB;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($id)
    {
        $idData = $id;
        return view('payment.index', ['id_user' => $idData]);
    }

    public function bayarCourse(Request $request)
    {
        $payment = Payment::where('registerde_user_id', '=', $request->id_user_registered)->get();
        $userRegistered = RegisteredUser::find($request->id_user_registered);

        foreach ($payment as $pay) {
            $dataPayment = $pay;
        }

        $userRegistered->status = 2;
        $dataPayment->status = 2;

        $userRegistered->save();
        $dataPayment->save();

        return \redirect('/dashboard')->with('info', 'Kamu telah melakukan pembayaran, sekarang kamu dapat mengakses materi.');
    }

    public function getUjianByStatus($id, $status)
    {
        $userID = auth()->user()->id;
        $score = DB::select('SELECT * FROM jawaban_users where peserta_id = ? AND score = ? AND materi_id = ?', [$userID, 1, $id]);

        $namaMateri = Materi::find($id);

        $trainers = DB::table('materis')
            ->join('jadwals', 'materis.jadwal_id', '=', 'jadwals.id')
            ->join('trainers', 'jadwals.trainer_id', '=', 'trainers.id')
            ->select('trainers.name')->get();

        foreach ($trainers as $tr) {
            $trainer = $tr;
        }

        $ujian = Ujian::where([['materi_id', '=', $id], ['status', '=', $status]])->paginate(1);
        return view('ujian.show', [
            'ujian' => $ujian,
            'materi' => $namaMateri,
            'status' => $status,
            'score' => $score,
            'trainer' => $trainer,
        ]);
    }

    public function searchUjianByStatus(Request $request)
    {
        $st = $request->status;
        $ujian = DB::table('ujians')
            ->join('materis', 'ujians.materi_id', '=', 'materis.id')
            ->select('ujians.*', 'materis.judul')
            ->where('ujians.status', '=', $st)
            ->get();

        return view('ujian.search', ['ujian' => $ujian]);
    }
}
