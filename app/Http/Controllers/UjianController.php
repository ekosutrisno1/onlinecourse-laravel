<?php

namespace App\Http\Controllers;

use App\Materi;
use App\Ujian;
use DB;
use Illuminate\Http\Request;

class UjianController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ujian = DB::table('ujians')
            ->join('materis', 'ujians.materi_id', '=', 'materis.id')
            ->select('ujians.*', 'materis.judul')
            ->paginate(10);

        $email = auth()->user()->email;
        $role = auth()->user()->role;

        if ($role != 2) {
            return view('ujian.index', ['ujian' => $ujian]);
        } else {
            return view('errorpage.error_custom')->with('message', 'Hai ' . $email . ' Kamu tidak mendapat izin mengakses halaman ini.');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $materi = Materi::all();
        return view('ujian.add', ['materi' => $materi]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ujian = new Ujian;

        $ujian->tanggal = $request->tanggal;
        $ujian->materi_id = $request->materi_id;
        $ujian->status = $request->status;
        $ujian->pertanyaan = $request->pertanyaan;
        $ujian->jawaban = $request->jawaban;

        $ujian->save();

        return redirect('/ujian')->with('success', 'Soal Ujian telah ditambah.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ujian  $ujian
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $userID = auth()->user()->id;
        $score = DB::select('SELECT * FROM jawaban_users where peserta_id = ? AND score = ? AND materi_id = ?', [$userID, 1, $id]);

        $namaMateri = Materi::find($id);

        $ujian = Ujian::where([['materi_id', '=', $id], ['status', '=', 1]])->paginate(1);

        return view('ujian.show',
            [
                'ujian' => $ujian,
                'materi' => $namaMateri,
                'score' => $score,
            ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ujian  $ujian
     * @return \Illuminate\Http\Response
     */
    public function edit(Ujian $ujian)
    {
        $ujian = Ujian::find($ujian->id);
        $materi = Materi::all();

        return view('ujian.edit', ['ujian' => $ujian, 'materi' => $materi]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ujian  $ujian
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ujian $ujian)
    {
        $ujian = Ujian::find($ujian->id);

        $ujian->tanggal = $request->tanggal;
        $ujian->materi_id = $request->materi_id;
        $ujian->status = $request->status;
        $ujian->pertanyaan = $request->pertanyaan;
        $ujian->jawaban = $request->jawaban;

        $ujian->save();

        return redirect('/ujian')->with('success', 'Soal Ujian telah diupdate.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ujian  $ujian
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ujian $ujian)
    {
        $ujianDelete = Ujian::find($ujian->id);
        $ujianDelete->delete();

        return redirect('/ujian')->with('success', 'Ujian Berhasil Dihapus!');
    }
}
