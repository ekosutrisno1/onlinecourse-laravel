<?php

namespace App\Http\Controllers;

use App\Role;
use App\Trainer;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserAccessController extends Controller
{
    public function index()
    {
        $trainer = Trainer::where('status', '=', 1)->orderBy('rating', 'DESC')->get();
        $user = User::where('role', '<>', 2)->orderBy('role', 'ASC')->paginate(5);
        return view('trainers.index', [
            'trainer' => $trainer,
            'user' => $user,
        ]);
    }

    public function create()
    {
        $role = Role::all();
        return view('trainers.add', ['role' => $role]);
    }

    public function store(Request $request)
    {

        $email = $request->email;
        $roleCek = $request->role;
        $name = $request->name;

        $valid = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'role' => ['required'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        if ($valid) {
            $user = new User;
            $user->name = $request->name;
            $user->role = $request->role;
            $user->email = $request->email;
            $user->email_verified_at = now();
            $user->password = Hash::make($request->password);
            $user->save();

            if ($roleCek == 3) {
                $userID = User::where('email', '=', $email)->get();
                foreach ($userID as $u) {}

                Trainer::create([
                    'name' => $name,
                    'email' => $email,
                    'status' => 1,
                    'rating' => 3,
                    'alamat' => 'Jakarta',
                    'no_hp' => '082371928902',
                    'user_id' => $u->id,
                ]);

            }
        }
        return back()->with('success', 'Admin baru berhasil ditambahkan.');
    }

    public function edit($id)
    {
        $role = Role::all();
        $dataToEdit = User::find($id);

        return view('trainers.edit', ['userData' => $dataToEdit, 'role' => $role]);

    }

    public function update(Request $request)
    {
        $roleLama = $request->role_lama;
        $roleSekarang = $request->role;
        $email = $request->email;

        $user = User::find($request->id);
        $user->role = $request->role;
        $user->save();

        if ($roleLama == 3) {
            $trainer = Trainer::where('email', '=', $email)->get();
            foreach ($trainer as $t) {}

            $t->status = 0;
            $t->save();
        } elseif ($roleSekarang == 3) {
            $trainer = Trainer::where('email', '=', $email)->get();
            foreach ($trainer as $t) {}

            $t->status = 1;
            $t->save();
        }

        return back()->with('success', 'Hak Akses ' . $user->name . ' berhasil diupdate.');
    }
}
