<?php

namespace App\Http\Controllers;

use App\JadwalUser;
use Illuminate\Http\Request;

class JadwalUserController extends Controller
{
    /**
         * Create a new controller instance.
         *
         * @return void
         */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return JadwalUser::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\JadwalUser  $jadwalUser
     * @return \Illuminate\Http\Response
     */
    public function show(JadwalUser $jadwalUser)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\JadwalUser  $jadwalUser
     * @return \Illuminate\Http\Response
     */
    public function edit(JadwalUser $jadwalUser)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\JadwalUser  $jadwalUser
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, JadwalUser $jadwalUser)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\JadwalUser  $jadwalUser
     * @return \Illuminate\Http\Response
     */
    public function destroy(JadwalUser $jadwalUser)
    {
        //
    }
}
