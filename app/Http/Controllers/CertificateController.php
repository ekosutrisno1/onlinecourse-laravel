<?php

namespace App\Http\Controllers;

use App\Jadwal;
use App\JadwalUser;
use App\RegisteredUser;
use App\Trainer;
use DB;

class CertificateController extends Controller
{
    public function index()
    {
        $email = auth()->user()->email;

        $user = RegisteredUser::where('email', '=', $email)->get();
        foreach ($user as $us) {}
        return view('payment.infoSertificate', ['infoUser' => $us]);
    }

    public function downloadPdf()
    {
        $email = auth()->user()->email;
        $nama = auth()->user()->name;
        $userID = auth()->user()->id;

        $user = RegisteredUser::where('email', '=', $email)->get();
        foreach ($user as $us) {}

        $userData = JadwalUser::where('registered_user_id', '=', $us->id)->get();
        foreach ($userData as $uD) {}

        $dataMateri = DB::select('SELECT * FROM materis WHERE jadwal_id = ?', [$uD->jadwal_id]);
        foreach ($dataMateri as $dM) {}

        $dataJadwal = Jadwal::find($uD->jadwal_id);
        $trainerName = Trainer::find($dataJadwal->trainer_id)->name;

        $materiID = 2;
        $score = DB::select('SELECT * FROM jawaban_users where peserta_id = ? AND score = ? AND materi_id = ?', [$userID, 1, $materiID]);

        return view('payment.certificate', [
            'kode' => $us->no_identitas,
            'name' => $nama,
            'materi' => $dM->judul,
            'score' => count($score),
            'trainer' => $trainerName,
            'tanggal' => date('d M Y'),
        ]);
    }
}
