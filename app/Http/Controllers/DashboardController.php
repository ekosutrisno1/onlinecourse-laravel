<?php

namespace App\Http\Controllers;

use App\Jadwal;
use App\JadwalUser;
use App\JawabanUser;
use App\RegisteredUser;
use App\Ujian;
use DB;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $email = auth()->user()->email;
        $role = auth()->user()->role;

        $dataJumlahPeserta = RegisteredUser::count();
        $dataJumlahPesertaMenjawabSoal = JawabanUser::count();
        $jumlahSoalHarian = Ujian::where('status', 1)->count();
        $jumlahSoalMingguan = Ujian::where('status', 0)->count();
        $jumlahSoalAkhir = Ujian::where('status', 2)->count();

        if ($role === 1 || $role === 3 || $role === 4) {
            return view('dashboard')->with([
                'peserta' => $dataJumlahPeserta,
                'jawaban' => $dataJumlahPesertaMenjawabSoal,
                'soalHarian' => $jumlahSoalHarian,
                'soalMingguan' => $jumlahSoalMingguan,
                'soalAkhir' => $jumlahSoalAkhir,
                'jadwal' => [],
                'materi' => [],
            ]);
        } else {

            $rUser = RegisteredUser::where('email', '=', $email)->get();
            $dataUser = null;
            foreach ($rUser as $dU) {
                $dataUser = $dU;
            }

            if ($dataUser) {
                $userData = JadwalUser::where('registered_user_id', '=', $dataUser->id)->get();
                foreach ($userData as $uD) {
                    $dataID = $uD;
                }

                $dataJadwal = Jadwal::find($dataID->jadwal_id);
                $dataMateri = DB::select('SELECT * FROM materis WHERE jadwal_id = ?', [$dataJadwal->id]);

                return view('dashboard', [
                    'jadwal' => $dataJadwal,
                    'materi' => $dataMateri,
                    'infoUser' => $dataUser]);
            }
        }
        return view('dashboard', ['jadwal' => [], 'materi' => []]);

    }
}
