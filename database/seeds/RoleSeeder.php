<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            [
                'name' => 'Administrator',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'User/Peserta',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Trainer/Dosen',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Management',
                'created_at' => now(),
                'updated_at' => now(),
            ],
        ]);
    }
}
