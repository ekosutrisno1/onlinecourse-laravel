<!DOCTYPE html>
<html>

<head>
    <link href="https://fonts.googleapis.com/css?family=Poppins:600&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/a81368914c.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/page-css/login.css')}}">

    <title>{{ config('app.name', 'OC-ExoApp') }} | Login</title>
</head>

<body>
    <img class="wave" src="{{asset('img/page-img/login/wave.png')}}">
    <div class="container">
        <div class="img">
            <img src="{{asset('img/page-img/login/bg.svg')}}">
        </div>
        <div class="login-content">
            <form method="POST" action="{{ route('login') }}">
                @csrf
                <img src=" {{asset('img/page-img/login/user-avatar.svg')}}">
                <h2 class="title">Welcome</h2>
                <div class="input-div one">
                    <div class="i">
                        <i class="fas fa-user"></i>
                    </div>
                    <div class="div">
                        <h5>Email</h5>
                        <input id="email" type="email" class="input" name="email" value="{{ old('email') }}" required
                            autocomplete="email" autofocus>
                    </div>
                </div>
                @error('email')
                <small role="alert" style="color: red">
                    <strong>{{ $message }}</strong>
                </small>
                @enderror
                <div class="input-div pass">
                    <div class="i">
                        <i class="fas fa-lock"></i>
                    </div>
                    <div class="div">
                        <h5>Password</h5>
                        <input type="password" class="input" name="password" required autocomplete="current-password">
                    </div>
                </div>
                @error('password')
                <small role="alert" style="color: red">
                    <strong>{{ $message }}</strong>
                </small>
                @enderror

                @if (Route::has('password.request'))
                <a href="{{ route('password.request') }}">
                    {{ __('Forgot Your Password?') }}
                </a>
                @endif

                <div>
                    <input class="r-me" type="checkbox" name="remember" id="remember"
                        {{ old('remember') ? 'checked' : '' }}>
                    <label class="r-me" for="remember">
                        {{ __('Remember Me') }}
                    </label>
                </div>
                <input type="submit" class="btn" value="{{ __('Login') }}">
                <a href="{{ route('register') }}">
                    {{ __('New? Register an Account') }}
                </a>
                <a href="/" style="margin-top: 20px">
                    <i class="fa fa-fw fa-home"></i> {{ __('Home') }}
                </a>
        </div>
        </form>
    </div>
    <script type="text/javascript" src="{{asset('js/page-js/login.js')}}"></script>
</body>

</html>
