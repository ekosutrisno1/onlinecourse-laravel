@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header bg-cst-dark text-white">{{ __('Verifikasi Alamat Email') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                    <div class="alert alert-success" role="alert">
                        {{ __('Link verifikasi baru telah dikirim ke Email anda.') }}
                    </div>
                    @endif

                    {{ __('Untuk melanjutkan proses, silahkan cek alamat email anda untuk meverifikasi.') }}
                    {{ __('Jika tidak menerima email, silahkan kirim ulang') }},
                    <br>
                    <a class="btn btn-outline-primary mt-3 float-right"
                        href="{{ route('verification.resend') }}">{{ __('Klik untuk mengirim ulang') }}
                        <i class="fa fa-fw fa-envelope"></i></a>
                    <br>
                    <span class="font-weight-bold mt-3">{{ __('ExoApp Team') }}</span>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
