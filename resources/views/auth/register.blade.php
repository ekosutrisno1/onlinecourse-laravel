{{-- @extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header bg-cst-dark text-white">{{ __('Register') }}</div>

<div class="card-body">
    <form method="POST" action="{{ route('register') }}">
        @csrf

        <div class="form-group row">
            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

            <div class="col-md-6">
                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name"
                    value="{{ old('name') }}" required autocomplete="name" autofocus>

                @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>

        <div class="form-group row">
            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

            <div class="col-md-6">
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email"
                    value="{{ old('email') }}" required autocomplete="email">

                @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>

        <div class="form-group row">
            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

            <div class="col-md-6">
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror"
                    name="password" required autocomplete="new-password">

                @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>

        <div class="form-group row">
            <label for="password-confirm"
                class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

            <div class="col-md-6">
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required
                    autocomplete="new-password">
            </div>
        </div>

        <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-4">
                <button type="submit" class="btn btn-primary">
                    {{ __('Register') }}
                </button>
            </div>
        </div>
    </form>
</div>
</div>
</div>
</div>
</div>
@endsection --}}

<!DOCTYPE html>
<html>

<head>
    <link href="https://fonts.googleapis.com/css?family=Poppins:600&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/a81368914c.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/page-css/login.css')}}">

    <title>{{ config('app.name', 'OC-ExoApp') }} | Register</title>
</head>

<body>
    <img class="wave" src="{{asset('img/page-img/login/wave.png')}}">
    <div class="container">
        <div class="img">
            <img src="{{asset('img/page-img/login/bg.svg')}}">
        </div>
        <div class="login-content">
            <form method="POST" action="{{ route('register') }}">
                @csrf
                <img src=" {{asset('img/page-img/login/user-avatar.svg')}}">
                <h2 class="title">Welcome</h2>
                <div class="input-div one">
                    <div class="i">
                        <i class="fas fa-user"></i>
                    </div>
                    <div class="div">
                        <h5>Nama Lengkap</h5>
                        <input id="name" type="text" class="input" name="name" value="{{ old('name') }}" required
                            autocomplete="name" autofocus>
                    </div>
                </div>
                @error('name')
                <small role="alert" style="color: red">
                    <strong>{{ $message }}</strong>
                </small>
                @enderror

                <div class="input-div one">
                    <div class="i">
                        <i class="fas fa-envelope"></i>
                    </div>
                    <div class="div">
                        <h5>Email</h5>
                        <input id="email" type="email" class="input" name="email" value="{{ old('email') }}" required
                            autocomplete="email" autofocus>
                    </div>
                </div>
                @error('email')
                <small role="alert" style="color: red">
                    <strong>{{ $message }}</strong>
                </small>
                @enderror

                <div class="input-div pass">
                    <div class="i">
                        <i class="fas fa-lock"></i>
                    </div>
                    <div class="div">
                        <h5>Password</h5>
                        <input type="password" class="input" name="password" required autocomplete="current-password">
                    </div>
                </div>
                @error('password')
                <small role="alert" style="color: red">
                    <strong>{{ $message }}</strong>
                </small>
                @enderror

                <div class="input-div pass">
                    <div class="i">
                        <i class="fas fa-lock"></i>
                    </div>
                    <div class="div">
                        <h5>Password Confirm</h5>
                        <input type="password" class="input" id="password-confirm" name="password_confirmation" required
                            autocomplete="new-password">
                    </div>
                </div>
                <input type="submit" class="btn" value="{{ __('Register') }}">
                <a href="{{ route('login') }}">
                    {{ __('Already Account? Login') }}
                </a>
            </form>
        </div>
    </div>
    <script type="text/javascript" src="{{asset('js/page-js/login.js')}}"></script>
</body>

</html>
