@extends('layouts.app')

@section('content')
<div class="container">
   <div class="row justify-content-center">
      <div class="col-md">
         <div class="card">
            <div class="card-header bg-cst-dark">
               @if (Gate::check('isAdmin'))
               <h5 class="text-white">Dashboard {{$peserta}} <i class="fa fa-fw fa-tachometer"></i></h5>
               @else
               <h5 class="text-white">Dashboard <i class="fa fa-fw fa-tachometer"></i></h5>
               @endif
            </div>
            <div class="card-body">
               @include('layouts.alert')
               <div class="row">
                  <div class="col-md-4 mb-2">
                     @if (Gate::check('isAdmin') || Gate::check('isDosen')|| Gate::check('isManager'))
                     @include('pages.admin-pages.isAdmin')
                     @else
                     @include('pages.user-pages.peserta')
                     @endif
                  </div>

                  <div class="col-md-8">
                     <div class="card mb-3">
                        <div class="row no-gutters">
                           <div class="col-md-4">
                              <img src="/img/avatar.png" class="card-img" alt="...">
                           </div>
                           <div class="col-md-8">
                              <div class="card-body">
                                 <h5 class="card-title">{{Auth::user()->name}}
                                    @switch(Auth::user()->role)
                                    @case(1)
                                    <span class="badge badge-primary float-right">ADMIN</span>
                                    @break
                                    @case(2)
                                    <span class="badge badge-primary float-right">USER</span>
                                    @break
                                    @case(3)
                                    <span class="badge badge-primary float-right">DOSEN/TRAINER</span>
                                    @break
                                    @case(4)
                                    <span class="badge badge-primary float-right">MANAGEMENT</span>
                                    @break
                                    @endswitch
                                 </h5>


                                 <p class="card-text">
                                    <small class="">Email :
                                       {{Auth::user()->email}}</small>
                                    @if (Gate::check('isUser'))
                                    @include('pages.user-pages.statusUser')
                                    @endif
                                 </p>
                                 <hr>
                                 <p class="card-text">This is a wider card with supporting text below as
                                    a
                                    natural lead-in to additional
                                    content. This content is a little bit longer.</p>
                                 <p class="card-text">
                                    <small class="text-muted">Account dibuat pada
                                       {{Auth::user()->created_at}}</small>
                                 </p>
                                 <p>
                                    @if ($materi)
                                    @foreach ($materi as $mat)
                                    <button type="button" class="btn btn-sm btn-success">
                                       <i class="fa fa-fw fa-user-graduate"></i>
                                       {{$infoUser->keterangan}}
                                    </button>
                                    @endforeach
                                    <button onclick="showDownloadSertificate()" type="button"
                                       class="btn btn-sm btn-primary">
                                       <i class="fa fa-fw fa-award"></i> Certificate
                                    </button>
                                    @endif
                                 </p>
                              </div>
                           </div>
                        </div>
                     </div>

                     @if ($jadwal)
                     <div id="accordion">
                        <div class="card mb-3">
                           <div class="card-header" id="heading{{$jadwal->kode_jadwal}}">
                              <h5 class="mb-0">
                                 <button class="btn collapsed" data-toggle="collapse"
                                    data-target="#collapse{{$jadwal->kode_jadwal}}" aria-expanded="false"
                                    aria-controls="collapse{{$jadwal->kode_jadwal}}">
                                    Detail Jadwal {{$jadwal->kode_jadwal}}
                                 </button>
                                 <small class="text-primary font-weight-bold">| ID:
                                    {{$infoUser->no_identitas}}</small>
                              </h5>
                           </div>

                           <div id="collapse{{$jadwal->kode_jadwal}}" class="collapse"
                              aria-labelledby="heading{{$jadwal->kode_jadwal}}" data-parent="#accordion">
                              <div class="card-body">
                                 <p class="font-weight-bold">
                                    <i class="fa fa-fw fa-graduation-cap"></i> Info User Jadwal
                                    {{$jadwal->kode_jadwal}}
                                    | status :
                                    @include('pages.user-pages.statusUser')
                                 </p>
                                 <p>
                                    <span>
                                       Email : <span class="font-weight-bold">{{$infoUser->email}}</span>
                                    </span><br>
                                    <span>
                                       No ID :
                                       <span class="font-weight-bold text-danger">{{$infoUser->no_identitas}}</span>
                                    </span><br>
                                    <span>
                                       Tempat lahir :
                                       <span class="font-weight-bold">{{$infoUser->tempat_lahir}}</span>
                                    </span><br>
                                    <span>
                                       Tanggal lahir :
                                       <span class="font-weight-bold">{{$infoUser->tanggal_lahir}}</span>
                                    </span><br>
                                    <span>
                                       Jenis Kelamin :
                                       @if ($infoUser->gender = 1)
                                       <span class="font-weight-bold">Laki-laki</span>
                                       @else
                                       <span class="font-weight-bold">Perempuan</span>
                                       @endif
                                    </span><br>
                                    <span>
                                       Alamat : <span class="font-weight-bold">{{$infoUser->alamat}}</span>
                                    </span><br>
                                    <span>
                                       Keterangan : <span class="font-weight-bold">{{$infoUser->keterangan}}</span>
                                    </span><br>

                                 </p>
                                 @if ($infoUser->status == 1)
                                 <a href="/payment/{{$infoUser->id}}" class="btn btn-sm btn-danger">
                                    <i class="fa fa-fw fa-shopping-cart"></i> Payment
                                 </a>
                                 @else
                                 <button class="btn btn-sm btn-primary">
                                    Payed <i class="fa fa-fw fa-check-double"></i>
                                 </button>
                                 @endif
                                 <p>
                                    <span class="badge badge-success lead">
                                       Terdaftar Mulai: {{$infoUser->created_at}}
                                       <i class="fa fa-fw fa-clandar"></i>
                                    </span>
                                    |
                                    <span class="badge badge-primary">
                                       Start at :{{$jadwal->start_date}}
                                       <i class="fa fa-fw fa-clandar"></i>
                                    </span>
                                    <span class="badge badge-primary">
                                       End At : {{$jadwal->end_date}}
                                       <i class="fa fa-fw fa-clandar"></i>
                                    </span>
                                    <br><br>
                                    <span class="font-weight-bold">Rating</span>
                                    <i class="fa fa-fw fa-star text-warning"></i>
                                    <i class="fa fa-fw fa-star text-warning"></i>
                                    <i class="fa fa-fw fa-star text-secondary"></i>
                                 </p>

                              </div>
                           </div>
                        </div>
                     </div>
                     @else
                     @if (Gate::check('isAdmin') || Gate::check('isDosen') || Gate::check('isManager'))
                     @include('pages.admin-pages.info')
                     @else
                     @include('pages.user-pages.info')
                     @endif
                     @endif
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
