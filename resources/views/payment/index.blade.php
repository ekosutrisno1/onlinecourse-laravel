@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md">
            <div class="card mx-auto" style=" width: 25rem">
                <div class="card-header">
                    <h5>Pembayaran Course <i class="fa fa-fw fa-shopping-cart"></i></h5>
                </div>

                <div class="card-body">
                    <img src="/img/pay.svg" alt="pay" width="200px" height="200px">
                    <form action="/payment/transaksi" method="POST">
                        @csrf
                        <input type="hidden" name="id_user_registered" value="{{$id_user}}">
                        <h3 class="panel-title d-none">
                            Payment Details
                        </h3>
                        <div class="form-group d-none">
                            <label for="cardNumber">
                                CARD NUMBER</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="cardNumber" placeholder="Nomor Kartu Valid"
                                    autofocus />
                            </div>
                        </div>
                        <div class="form-group d-none">
                            <label for="expired-card">TANGGAL KADALUARSA</label>
                            <div class="input-group w-50">
                                <input type="text" class="form-control" id="expired-card" placeholder="YYYY-MM-DD"
                                    autofocus />
                            </div>
                        </div>
                        <br>
                        <a class="btn btn-primary mb-3" href="#.">
                            Total Pembayaran
                            <span class="badge badge-pill badge-light">
                                <span class="glyphicon glyphicon-usd">

                                </span>Rp. 180.000</span>
                        </a>

                        <br />
                        <button class="btn btn-success btn-lg btn-block" type="submit">Bayar</a>

                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
