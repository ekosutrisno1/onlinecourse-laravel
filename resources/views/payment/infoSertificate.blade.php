<div class="row">
    <div class="col mx-2">
        <h4>Eksho: <strong><span class="text-primary">Info untuk hasil terbaik</span></strong></h4>
        <p class="lead">Status:
            @if ($infoUser->status === 5)
            <span class="small text-success">Selamat kamu telah Lulus, dan dapat mengambil certificate!</span>
            @else
            <span class="small text-danger">Kamu belum lulus dan tidak dapat mengambil certificate!</span>
            @endif
        </p>
        <p class="lead">
            <li>Gunakan Browser Chrome</li>
            <li>Kemudian Click Kanan</li>
            <li>Pilih Print</li>
            <li>Pilih Destination [Save as PDF]</li>
            <li>Atur Layout Lanscape</li>
            <li>Margin default</li>
            <li>Atur Ukuran kertas [A4 / Letter]</li>
            <li>Scale default</li>
            <li>Centang Background Graphic</li>
            <li>Lalu <strong>Save</strong></li>

        </p>
        <div class="col">
            <a href="/certificate"
                class="{{($infoUser->status === 5) ? 'btn btn-sm btn-primary float-right' : 'btn btn-sm btn-primary float-right disabled'}}">
                Ambil Certificate <i class="fa fa-fw fa-download"></i>
            </a>
        </div>
    </div>
</div>
