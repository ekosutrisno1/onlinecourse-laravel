<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>Certificate_{{$name}}</title>

   <link rel="dns-prefetch" href="//fonts.gstatic.com">
   <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
   <style>
      .signature,
      .title {
         float: left;
         border-top: 1px solid #000;
         width: 200px;
         text-align: center;
      }

      .bg-img {
         background-image: url('img/olim.svg');
         background-repeat: no-repeat;
         background-size: cover;
         background-position: center;
      }
   </style>
</head>

<body>
   <center>
      <div class="bg-img"
         style="width:800px; height:600px; padding:20px; text-align:center; border: 10px solid #019587">
         <div style="width:750px; height:550px; padding:20px; text-align:center; border: 5px solid #435B71">

            <span style="font-size:38px; font-weight:bold; font-family: 'Nunito';">Certificate of Completion</span>
            <br><br>
            <span style="font-size:25px;"><i>This is to certify that</i></span>
            <br><br>
            <span style="font-size:25px;font-family: 'Nunito';color: rgb(217, 255, 242);">
               <b>{{$name}}</b></span><br /><br />
            <span style="font-size:25px"><i>has completed the course in Eksho</i></span> <br /><br />
            <span style="font-size:25px;font-family: 'Nunito';">{{$materi}}</span> <br /><br />
            <span style="font-size:20px">
               user ID: <b>{{$kode}}</b></span>
            <br />
            <span style="font-size:20px">with score of <b>{{$score}}%</b></span> <br /><br /><br>
            <span style="font-size:25px;color: rgb(217, 255, 242);"><i>Completed Date</i></span><br>
            <span style="font-size:25px;color: rgb(217, 255, 242);"><i>{{$tanggal}}</i></span><br>
            <table style="margin-top:30px;float:left">
               <tr>
                  <td><span style="color: white;font-family: 'Nunito';"><b>{{$trainer}}</b></td>
               </tr>
               <tr>

                  <td style="width:200px;float:left;border:0;border-bottom:1px solid #000;"></td>
               </tr>
               <tr>
                  <td style="text-align:center"><span><b>Trainer/Dosen</b></td>
               </tr>
            </table>
            <table style="margin-top:30px;float:right">
               <tr>
                  <td><span style="color: white;font-family: 'Nunito';"><b>{{$name}}</b></td>
               </tr>
               <tr>
                  <td style="width:200px;float:right;border:0;border-bottom:1px solid #000;"></td>
               </tr>
               <tr>
                  <td style="text-align:center"><span><b>Peserta</b></td>
               </tr>
            </table>
         </div>
      </div>
   </center>
</body>

</html>
