<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>OC-ExoApp</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <script src="https://kit.fontawesome.com/c4c4a7fc05.js"></script>

    <!-- Styles -->
    <link rel="stylesheet" href="/css/app.css">
    <link rel="stylesheet" href="/css/welcome.css">
    <style>
        html,
        body {
            background-image: url(img/bg04.svg);
        }
    </style>
</head>

<body>
    <div class="flex-center position-ref full-height">
        @if (Route::has('login'))
        <div class="top-right links">
            @auth
            <a href="{{ url('/dashboard') }}">Dashboard <i class="fa fa-rocket"></i></a>
            @else
            <a href="{{ route('login') }}">Login <i class="fa fa-sign-in-alt"></i></a>

            @if (Route::has('register'))
            <a href="{{ route('register') }}">Register <i class="fa fa-user-graduate"></i></a>
            @endif
            @endauth
        </div>
        @endif

        <div class="content">
            <div>
                <img src="{{asset('img/logo/as.png')}}" width="100px">
                <p class="m-b-ms">Welcome to</p>
            </div>
            <div class="title">
                OC-ExoApp <i class="fa fa-graduation-cap"></i>
            </div>
            <p>Kami menyediakan Kursus Online Matemtika untuk Jenjang SD, SMP dan SMA</p>

            @if (!Auth::user())
            <a class="btn btn-primary btn-sm mb-2" href="{{ route('register') }}">
                Yuk Gabung dan belajar bersama kami <i class="fa fa-fw fa-user"></i>
            </a>
            @endif

            <div class="links">
                <a href="#.">About Us <i class="fa fa-question-circle"></i></a>
                <a href="#.">Contact <i class="fa fa-phone-square"></i></a>
            </div>
        </div>
    </div>
</body>

</html>
