<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'OC-ExoApp') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="https://kit.fontawesome.com/c4c4a7fc05.js"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/sweetalert2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
</head>

<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-dark bg-cst-dark shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="{{asset('img/logo/eksho.png')}}" alt="exoapp-logo" width="110px">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}
                                <i class="fa fa-sign-in-alt"></i>
                            </a>
                        </li>
                        @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}
                                <i class="fa fa-user-graduate"></i>
                            </a>
                        </li>
                        @endif
                        @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                <i class="fa fa-fw fa-user-graduate"></i> {{ Auth::user()->name }}
                                <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="/dashboard">Dashboard <i
                                        class="fa fa-tachometer"></i></a>
                                @if (!Gate::check('isUser'))
                                <a class="dropdown-item" href="/admin">
                                    Admin <i class="fa fa-fw fa-shield"></i>
                                </a>
                                @endif
                                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    Logout <i class="fa fa-sign-in-alt"></i>
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                    style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
        <nav class="navbar navbar-expand-md navbar-dark bg-csr-green shadow-sm" style="height: 40px">
            <div class="container">
                @if (Gate::check('isAdmin') || Gate::check('isManager')|| Gate::check('isDosen'))
                <ul class="navbar-nav mr-auto">
                    <a class="nav-item nav-link" href="/jadwal">
                        Jadwal <i class="fa fa-fw fa-calendar"></i>
                    </a>
                    <a class="nav-item nav-link" href="/materi">
                        Tema <i class="fa fa-fw fa-tasks"></i>
                    </a>
                    <a class="nav-item nav-link" href="/ujian">
                        Ujian <i class="fa fa-fw fa-question"></i>
                    </a>
                    <a class="nav-item nav-link" href="/registered_user">
                        Data Peserta <i class="fa fa-fw fa-user-graduate"></i>
                    </a>
                    <a class="nav-item nav-link" href="/jawaban_user">
                        Data Jawaban <i class="fa fa-fw fa-pencil"></i>
                    </a>
                    @if (Gate::check('isAdmin') || Gate::check('isManager'))
                    <a class="nav-item nav-link" href="/access">
                        Access Controll <i class="fa fa-fw fa-table"></i>
                    </a>
                    @endif
                </ul>
                @endif
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modal-app" tabindex="-1" role="dialog" aria-labelledby="modal-appLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header text-white bg-csr-green">
                    <img src="{{asset('img/logo/as.png')}}" width="35px">
                    <h5 class="modal-title ml-3" id="modal-appLabel"></h5>
                    <small class="ml-2">Co-ExoApp Modal</small>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="data-form-content">
                    ...
                </div>
                <div class="modal-footer">
                    <small class="float-left">Co-ExoApp Version 1.0.0</small>
                </div>
            </div>
        </div>
    </div>

    @include('layouts.footer')

    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script src="{{ asset('js/sweetalert2.all.min.js') }}"></script>
    <script src="{{ asset('js/script.js') }}"></script>
</body>

</html>
