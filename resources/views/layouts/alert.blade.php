@if (session('status'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <strong>Yee Berhasil </strong> {{session('status')}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

@if (session('info'))
<div class="alert alert-success" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <h4 class="alert-heading">Well done!</h4>
    Terima Kasih <span class="font-weight-bold">{{Auth::user()->email}}</span> <br>
    <p>{{session('info')}}</p>
    <hr>
    <p class="mb-0">
        Semua syarat dan ketentuan berlaku, <br>
        Terimakasih,
        <br>
        <br>
        <span class="font-weight-bold">Team Support ExoApp</span>
    </p>

</div>
@endif
