<footer>
    <div class="footer-copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <img src="{{asset('img/logo/as.png')}}" width="35px">
                    <p>Copyright <span class="font-weight-bold">CO-ExoApp © 2020</span>. All rights reserved.</p>
                </div>
            </div>
        </div>
    </div>
</footer>
