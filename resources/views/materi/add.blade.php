<form action="/materi" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label for="judul">Judul Materi</label>
        <input type="text" name="judul" class="form-control" id="judul" placeholder="Judul...">
    </div>
    <div class="form-group">
        <label for="tanggal">Tanggal</label>
        <input type="text" name="tanggal" class="form-control" id="tanggal">
    </div>
    <div class="row">
        <div class="col-md">
            <div class="form-group">
                <label for="jadwal_id">Jadwal</label>
                <select type="text" name="jadwal_id" class="form-control" id="jadwal_id">
                    <option value="" selected="selected">Pilih Jadwal</option>
                    @foreach ($jadwal as $jdw)
                    <option value="{{$jdw->id}}">{{$jdw->kode_jadwal}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md">
            <div class="form-group">
                <label for="status">Status</label>
                <select type="text" name="status" class="form-control" id="status">
                    <option value="1" selected="selected">Open</option>
                    <option value="0">Close</option>
                    <option value="2">On Schedule</option>
                </select>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="attachment">Attachment</label>
        <input type="file" name="attachment" class="form-control-file" id="attachment">
    </div>
    <div class="form-group">
        <label for="sinopsis">Sinopsis</label>
        <textarea placeholder="Tulis Sinopsis..." rows="4" name="sinopsis" class="form-control"
            id="sinopsis"></textarea>
    </div>
    <button type="submit" class="btn btn-primary ml-3 float-right">Submit</button>
    <button type="reset" class="btn btn-outline-primary float-right">Reset</button>
</form>
