<form action="/materi/{{$materi->id}}" method="POST" enctype="multipart/form-data">
    @csrf
    <input type="hidden" name="id" value="{{$materi->id}}">
    {{ method_field('PUT') }}
    <div class="form-group">
        <label for="judul">Judul Materi</label>
        <input value="{{$materi->judul}}" type="text" name="judul" class="form-control" id="judul">
    </div>
    <div class="form-group">
        <label for="tanggal">Tanggal</label>
        <input value="{{$materi->tanggal}}" type="text" name="tanggal" class="form-control" id="tanggal">
    </div>
    <div class="row">
        <div class="col-md">
            <div class="form-group">
                <label for="jadwal_id">Jadwal</label>
                <select type="text" name="jadwal_id" class="form-control" id="jadwal_id">
                    <option value="" selected="selected">Pilih Jadwal</option>
                    @foreach ($jadwal as $jdw)
                    @if ($materi->jadwal_id == $jdw->id)
                    <option value="{{$jdw->id}}" selected="selected">{{$jdw->kode_jadwal}}</option>
                    @endif
                    <option value="{{$jdw->id}}">{{$jdw->kode_jadwal}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md">
            <div class="form-group">
                <label for="status">Status</label>
                <select type="text" name="status" class="form-control" id="status">
                    @switch($materi->status)
                    @case(1)
                    <option value="1" selected="selected">Open</option>
                    <option value="0">Close</option>
                    <option value="2">On Schedule</option>
                    @break
                    @case(0)
                    <option value="1">Open</option>
                    <option value="0" selected="selected">Close</option>
                    <option value="2">On Schedule</option>
                    @break
                    @case(2)
                    <option value="1">Open</option>
                    <option value="0">Close</option>
                    <option value="2" selected="selected">On Schedule</option>
                    @break
                    @endswitch
                </select>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="attachment">Attachment</label>
        <input value="{{$materi->attachment}}" type="file" name="attachment" class="form-control-file" id="attachment">
    </div>
    <div class="form-group">
        <label for="sinopsis">Sinopsis</label>
        <textarea rows="4" name="sinopsis" class="form-control" id="sinopsis">{{$materi->sinopsis}}</textarea>
    </div>
    <button type="submit" class="btn btn-primary ml-3 float-right">Submit</button>
    <button type="reset" class="btn btn-outline-primary float-right">Reset</button>
</form>
