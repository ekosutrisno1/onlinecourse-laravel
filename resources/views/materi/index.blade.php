@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <h6 class="text-center display-4 ml-2">Tema Page <span style=" color:#052b94">
                <i class="fa fa-fw fa-tasks"></i></span></h6>
        <div class="col">
            @if (session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>Yee Berhasil </strong> {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif
        </div>
    </div>

    <div class="row">
        <div class="col">
            <p>
                Jadwal course yang disediakan di <span class="font-weight-bold text-primary">CO-ExoApp</span> telah
                mengikuti dan memenuhi
                standar kebutuhan
                industri,sehinggga lulusan <span class="font-weight-bold text-primary">CO-ExoApp</span> mempunyai
                pleuang yang besar untuk siap terjun kednuia kerja.
            </p>
            <button class="btn btn-primary" onclick="addMateri()">Add Tema Course
                <i class="fa fa-fw fa-plus"></i></button>
        </div>
    </div>
    <hr>

    @if (count($materi)> 0)
    <div class="row">
        <div class="d-flex justify-content-center flex-wrap">
            @foreach ($materi as $i => $mat)
            <div class="card mx-4 mb-4 shadow-sm zoom-card-materi" style="width:20rem;">
                <img src="{{ asset('/img/bg03.svg') }}" class="card-img-top" alt="materi-img">
                <div class="card-body">
                    <h3 class="card-title">{{$mat->judul}} |
                        @if ($mat->status == 1)
                        <span class="font-weight-bold text-success">Open <i class="fa fa-fw fa-folder-open"></i></span>
                        @else
                        <span class="font-weight-bold text-danger">Close <i class="fa fa-fw fa-folder"></i></span>
                        @endif
                    </h3>

                    <p class="card-text">{{Str::limit($mat->sinopsis, 200,'...')}}</p>
                    <p class="font-weight-bold">Tanggal {{$mat->tanggal}} <i
                            class="fa fa-fw fa-calendar-alt text-success"></i></p>

                    <p>
                        <a href="/materi/{{$mat->id}}" class="btn btn-primary btn-sm">Detail Materi <i
                                class="fa fa-fw fa-list"></i></a>
                        <button onclick="editMateri({{$mat->id}})" class="btn btn-primary btn-sm">Edit
                            <i class="fa fa-fw fa-edit"></i></button>
                        <button type="button" class="btn btn-primary btn-sm" onclick="deleteMateri({{$mat->id}})">Hapus
                            <i class="fa fa-fw fa-trash"></i></button>
                    </p>
                </div>
            </div>
            @endforeach
            <form class="d-none" id="delete-form" action="" method="POST">
                {{ method_field('DELETE') }}
                @csrf
            </form>
        </div>
    </div>
    @else
    <div class="alert alert-primary alert-dismissible fade show" role="alert">
        <strong>Tidak ada Tema yang tersedia.</strong>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="d-flex align-content-center">
        <img class="align-self-auto" src="/img/no-data.svg" width="300px">
    </div>

    @endif
</div>
@endsection
