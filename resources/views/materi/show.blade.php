@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <h3 class="text-center font-weight-bold ml-3">
            <i class="fa fa-fw fa-circle small"></i> Detail Tema {{$materi->judul}}
            <span style="color:#9e0356"><i class="fa fa-fw fa-search"></i></span>
        </h3>
    </div>

    <div class="row">
        <div class="col">
            <p>
                Jadwal course yang disediakan di <span class="font-weight-bold text-primary">CO-ExoApp</span> telah
                mengikuti dan memenuhi
                standar kebutuhan
                industri,sehinggga lulusan <span class="font-weight-bold text-primary">CO-ExoApp</span> mempunyai
                pleuang yang besar untuk siap terjun kednuia kerja.
            </p>
            <a href="javascript:history.back()" class="btn btn-primary">
                <i class="fa fa-fw fa-arrow-left"></i> Back to Tema</a>
        </div>
    </div>
    <hr>


    <div class="row">
        <div class="col-md">
            <div class="card mx-4 mb-4 shadow-sm">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <img src="/img/reset-psw.svg" width="100%">
                        </div>
                        <div class="col-md-8">
                            <h3>
                                {{$materi->judul}} |
                                <span class="font-weight-bold" style="color:#052b94">{{$materi->kode_jadwal}}</span>
                            </h3>
                            <p class="lead font-weight-bold">
                                <span class="font-weight-bold text-success">
                                    Trainer |
                                </span>
                                <span class="font-weight-bold text-primary">
                                    {{$materi->name}} <i class="fa fa-fw fa-user-shield"></i>
                                </span>
                            </p>
                            <p class="font-weight-bold">Tanggal {{$materi->tanggal}}</p>
                            <p>{{$materi->sinopsis}}</p>
                            <h5>@if ($materi->status == 1)
                                Status: <span class="font-weight-bold text-success">Open
                                    <i class="fa fa-fw fa-folder-open"></i></span>
                                @else
                                Status: <span class="font-weight-bold text-danger">Close
                                    <i class="fa fa-fw fa-folder"></i></span>
                                @endif
                            </h5>
                            <hr>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <hr>
    <div class="row">
        <h3 class="text-center font-weight-bold ml-3">
            <i class="fa fa-fw fa-circle small"></i> Materi Untuk Tema {{$materi->judul}}
            <span style="color:#9e0356"><i class="fa fa-fw fa-edit"></i></span></h3>
    </div>

    <div class="row">
        <div class="col">
            <p>
                Jadwal course yang disediakan di <span class="font-weight-bold text-primary">CO-ExoApp</span> telah
                mengikuti dan memenuhi
                standar kebutuhan
                industri,sehinggga lulusan <span class="font-weight-bold text-primary">CO-ExoApp</span> mempunyai
                pleuang yang besar untuk siap terjun kednuia kerja.
            </p>
        </div>
    </div>
    <hr>

    <div class="row">
        <div class="d-flex justify-content-center flex-wrap">
            @for ($i = 1; $i <= 17; $i++) <div id="accordion">
                <div class="card mx-3 mb-3" style="width:20rem;">
                    <div class="card-header" id="heading{{$i}}">
                        <h5 class="mb-0">
                            <button class="btn collapsed" data-toggle="collapse" data-target="#collapse{{$i}}"
                                aria-expanded="false" aria-controls="collapse{{$i}}">
                                MAT {{$materi->judul}}-{{$i}}
                            </button>
                            <small class="text-primary font-weight-bold">| D 20 Menit</small>
                        </h5>
                    </div>

                    <div id="collapse{{$i}}" class="collapse" aria-labelledby="heading{{$i}}" data-parent="#accordion">
                        <div class="card-body">
                            <p class="font-weight-bold">
                                <i class="fa fa-fw fa-graduation-cap"></i> CO-ExoApp Chapter {{$i}}
                            </p>
                            <p>
                                Lorem ipsum dolor, sit amet consectetur adipisicing elit. Laborum, dignissimos quidem?
                                Iure et laudantium ratione adipisci harum, maxime debitis sapiente modi nisi, odit
                                voluptate facere provident aperiam vitae dolore inventore.
                                <br><br>
                                <a class="btn btn-outline-primary btn-sm" href="#!">Jawab Soal Harian</a>
                                <a class="btn btn-primary btn-sm" href="#!">Baca <i class="fa fa-fw fa-pencil"></i></a>
                                <br><br>
                                <span class="font-weight-bold">Rating</span>
                                <i class="fa fa-fw fa-star text-warning"></i>
                                <i class="fa fa-fw fa-star text-warning"></i>
                                <i class="fa fa-fw fa-star text-warning"></i>
                                <i class="fa fa-fw fa-star text-warning"></i>
                                <i class="fa fa-fw fa-star text-secondary"></i>
                            </p>
                        </div>
                    </div>
                </div>
        </div>
        @endfor
    </div>
</div>
</div> @endsection
