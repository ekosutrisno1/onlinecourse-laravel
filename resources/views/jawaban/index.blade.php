@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col">
                            <h2 class="display-4">
                                Jawaban User
                                <i class="fa fa-graduation-cap"></i>
                            </h2>
                            <p>
                                Berisi semua Data jawaban semua Peserta course yang disediakan di <span
                                    class="font-weight-bold text-primary">CO-ExoApp</span> telah
                                mengikuti dan memenuhi
                                standar kebutuhan
                                industri,sehinggga lulusan <span class="font-weight-bold text-primary">CO-ExoApp</span>
                                mempunyai
                                pleuang yang besar untuk siap terjun kedunia kerja.
                            </p>
                            <h3>
                                Total jawaban: <span class="badge badge-success">
                                    {{$jawaban->total()}}
                                    Jawaban</span>
                            </h3>
                            <br>
                            <a href="javascript:history.back()" class="btn btn-primary btn-sm">
                                <i class="fa fa-fw fa-arrow-left"></i> Back</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    @if (session('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>Yee Berhasil </strong> {{session('success')}}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif
                    <table class="table table-hover table-responsive w-100">
                        <thead>
                            <tr>
                                <th>Jawaban</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($jawaban as $i => $jawab)
                            <tr>
                                <td>
                                    <span class="font-weight-bold text-primary lead">
                                        <i class="fa fa-paperclip"></i> {{$jawab->judul}}
                                    </span>
                                    |
                                    @if ($jawab->score == 1)
                                    <span class="font-weight-bold badge badge-success">
                                        Score 1
                                    </span>
                                    @else
                                    <span class="font-weight-bold badge badge-danger">
                                        Score 0
                                    </span>
                                    @endif
                                    <br>
                                    <span class="font-weight-bold">
                                        Jawaban:
                                    </span>
                                    {{$jawab->jawaban}}
                                    <br>
                                    <br>
                                    Koreksi:
                                    @if ($jawab->score == 1)
                                    <span class="font-weight-bold text-success">
                                        Benar <i class="fa fa-check-circle"></i>
                                    </span>
                                    @else
                                    <span class="font-weight-bold text-danger">
                                        Salah <i class="fa fa-times-circle"></i>
                                    </span>
                                    @endif
                                    <br>
                                    <span class="font-weight-bold text-danger small">Dijawab Pada: </span>
                                    <span class="font-weight-bold text-primary small">{{$jawab->created_at}}</span>
                                    |
                                    <span class="font-weight-bold text-danger small">Oleh: </span>
                                    <span class="font-weight-bold text-primary small">{{$jawab->name}}</span>
                                    <span class="font-weight-bold text-success small"><i class="fa fa-check"></i></span>
                                    <br>
                                    <span class="font-weight-bold text-danger small">Status Soal: </span>
                                    <span class="font-weight-bold text-primary small">
                                        @if ($jawab->status_soal == 1)
                                        <span class="font-weight-bold text-success">Harian
                                            <i class="fa fa-fw fa-calendar-check"></i>
                                        </span>
                                        @elseif($jawab->status_soal == 0)
                                        <span class="font-weight-bold text-danger">Mingguan
                                            <i class="fa fa-fw fa-list"></i></span>
                                        @else
                                        <span class="font-weight-bold text-primary">Akhir Tema
                                            <i class="fa fa-fw fa-graduation-cap"></i></span>
                                        @endif
                                    </span>
                                    |
                                    <span class="font-weight-bold text-danger small">Nomor Soal: </span>
                                    <span class="font-weight-bold text-primary small">{{$jawab->nomor_soal}}</span>
                                    <span class="font-weight-bold text-success small">
                                        <i class="fa fa-check-circle"></i></span>
                                </td>

                                <td>
                                    <button class="btn btn-sm btn-danger" type="button"
                                        onclick="deleteJawaban({{$jawab->id}})">
                                        Hapus <i class="fa fa-fw fa-trash"></i>
                                    </button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <form class="d-none" id="delete-form" action="" method="POST">
                        {{ method_field('DELETE') }}
                        @csrf
                    </form>
                    <hr>
                    <div class="py-2 float-right">
                        {{$jawaban->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
