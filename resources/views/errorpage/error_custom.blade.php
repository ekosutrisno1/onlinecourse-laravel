<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Unauthorized | OC-ExoApp</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script src="https://kit.fontawesome.com/c4c4a7fc05.js"></script>
    <!-- Styles -->
    <style>
        html,
        body {
            background-image: url(img/bg04.svg);
            background-size: cover;
            background-repeat: no-repeat;
            background-color: #f8fafc;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links>a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }

        .m-b-ms {
            font-size: 40px;
            margin-bottom: 15px;
        }
    </style>
</head>

<body>
    <div class="flex-center position-ref full-height">
        <div class="content">

            <div class="title">
                Unauthorized <i class="fa fa-user-lock"></i>
            </div>
            <p>Halo {{Auth::user()->name}}, Kamu tidak dizinkan mengakses halaman ini, silahkan kembali
                <br>
                <a class="btn btn-primary my-3" href="javascript:history.back()"><i class="fa fa-arrow-left"></i>
                    Kembali</a>
            </p>


            <div class="links">
                <a href="https://laravel.com/docs">About Us <i class="fa fa-question-circle"></i></a>
                <a href="https://laracasts.com">Contact <i class="fa fa-phone-square"></i></a>
            </div>
        </div>
    </div>
</body>

</html>
