@extends('layouts.app')

@section('content')
<div class="container">
   <div class="row justify-content-center">
      <div class="col-md">
         <div class="card">
            <div class="card-header bg-cst-dark">
               <h5 class="text-white">Dashboard <i class="fa fa-fw fa-tachometer"></i>
                  <span class="badge badge-primary float-right">ADMIN</span>
               </h5>
            </div>
            <div class="card-body">
               @if (session('status'))
               <div class="alert alert-success alert-dismissible fade show" role="alert">
                  <strong>Yee Berhasil </strong> {{session('status')}}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                  </button>
               </div>
               @endif

               <div class="d-flex justify-content-center flex-wrap">
                  <a href="/jadwal" style="text-decoration: none">
                     <div class="d-flex justify-content-center flex-wrap">
                        <div class="card mx-4 mb-4 shadow-sm zoom-card-jadwal" style="width: 18rem; height: 20rem">
                           <img src="/img/bg01.svg" class="card-img-top" alt="...">
                           <div class="card-body">
                              <p class="card-text text-center">
                                 Semua tentang jadwal dan detail tema yang tersedia.
                              </p>
                              <h3 class="card-title text-center">Jadwal Kursus
                                 <i class="fa fa-fw fa-calendar"></i></h3>
                           </div>
                        </div>
                     </div>
                  </a>
                  <a href="/materi" style="text-decoration: none">
                     <div class="d-flex justify-content-center flex-wrap">
                        <div class="card mx-4 mb-4 shadow-sm zoom-card-materi" style="width: 18rem; height: 20rem">
                           <img src="/img/bg03.svg" class="card-img-top" alt="...">
                           <div class="card-body">
                              <h3 class="card-title text-center">Tema Materi
                                 <i class="fa fa-fw fa-tasks"></i></h3>
                           </div>
                        </div>
                     </div>
                  </a>
                  <a href="/ujian" style="text-decoration: none">
                     <div class="d-flex justify-content-center flex-wrap">
                        <div class="card mx-4 mb-4 shadow-sm zoom-card-jadwal-show" style="width: 18rem; height: 20rem">
                           <img src="/img/soal.svg" class="card-img-top" alt="...">
                           <div class="card-body">
                              <h3 class="card-title text-center">Bank Soal
                                 <i class="fa fa-fw fa-question"></i></h3>
                           </div>
                        </div>
                     </div>
                  </a>
                  <a href="/registered_user" style="text-decoration: none">
                     <div class="d-flex justify-content-center flex-wrap">
                        <div class="card mx-4 mb-4 shadow-sm zoom-card-user" style="width: 18rem; height: 20rem">
                           <img src="/img/user.svg" class="card-img-top" alt="...">
                           <div class="card-body">
                              <h3 class="card-title text-center">Data Peserta
                                 <i class="fa fa-fw fa-user-graduate"></i></h3>
                           </div>
                        </div>
                     </div>
                  </a>
                  <a href="/jawaban_user" style="text-decoration: none">
                     <div class="d-flex justify-content-center flex-wrap">
                        <div class="card mx-4 mb-4 shadow-sm zoom-card-jawaban" style="width: 18rem; height: 20rem">
                           <img src="/img/jawaban.svg" class="card-img-top" alt="...">
                           <div class="card-body">
                              <h3 class="card-title text-center">Data Jawaban
                                 <i class="fa fa-fw fa-pencil"></i></h3>
                           </div>
                        </div>
                     </div>
                  </a>
                  @can('isAdmin')
                  <a href="/access" style="text-decoration: none">
                     <div class="d-flex justify-content-center flex-wrap">
                        <div class="card mx-4 mb-4 shadow-sm zoom-card-trainer" style="width: 18rem; height: 20rem">
                           <img src="/img/dev.svg" class="card-img-top" alt="...">
                           <div class="card-body">
                              <p class="card-text text-center">
                                 Semua tentang Trainer dan detail jadwal yang ditangani.
                              </p>
                              <h3 class="card-title text-center">Access Controll
                                 <i class="fa fa-fw fa-table"></i></h3>
                           </div>
                        </div>
                     </div>
                  </a>
                  @endcan
               </div>

            </div>
         </div>
      </div>
   </div>
</div>
@endsection
