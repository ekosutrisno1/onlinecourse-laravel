<form action="/registered_user" method="POST">
    @csrf
    <input type="hidden" name="jadwal_id" id="jadwal_id">
    <h4>Email: {{Auth::user()->email}}</h4>
    <div class="form-group">
        <label for="tempat_lahir">Tempat Lahir</label>
        <input type="text" name="tempat_lahir" class="form-control" id="tempat_lahir" placeholder="Tempat lahir..."
            required>
    </div>
    <div class="row">
        <div class="col">
            <div class="form-group">
                <label for="tanggal_lahir">Tanggal Lahir</label>
                <input type="text" name="tanggal_lahir" class="form-control" id="tanggal_lahir"
                    placeholder="Tanggal lahir..." required>
            </div>
        </div>
        <div class="col">
            <div class="form-group">
                <label for="gender">Jenis Kelamin</label>
                <select type="text" name="gender" class="form-control" id="gender" required>
                    <option value="" selected="selected">Pilih gender...</option>
                    <option value="1">Laki-laki</option>
                    <option value="0">Perempuan</option>
                </select>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label for="alamat">Alamat</label>
        <textarea placeholder="Tulis alamat..." rows="3" name="alamat" class="form-control" id="alamat"
            required></textarea>
    </div>
    <div class="form-group">
        <label for="keterangan">Keterangan</label>
        <textarea placeholder="Tulis keterangan..." rows="3" name="keterangan" class="form-control" id="keterangan"
            required></textarea>
    </div>
    <button type="submit" class="btn btn-primary float-right">Submit</button>
    <button type="button" class="btn btn-outline-primary float-right mr-3" data-dismiss="modal">Batal</button>
</form>
