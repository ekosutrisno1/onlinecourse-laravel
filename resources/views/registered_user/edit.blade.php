<form action="/registered_user/{{$userEdit->id}}" method="POST">
    @csrf
    {{ method_field('PUT') }}
    <input type="hidden" name="id" value="{{$userEdit->id}}">
    <input type="hidden" name="jadwal_id" id="jadwal_id">
    <h4>Email: {{$userEdit->email}}</h4>
    <div class="row">
        <div class="col">
            <div class="form-group">
                <label for="tempat_lahir">Tempat Lahir</label>
                <input value="{{$userEdit->tempat_lahir}}" type="text" name="tempat_lahir" class="form-control"
                    id="tempat_lahir" placeholder="Tempat lahir..." required>
            </div>
        </div>
        <div class="col">
            <div class="form-group">
                <label for="status">Status</label>
                <select type="text" name="status" class="form-control" id="status" required>
                    @switch($userEdit->status)
                    @case(1)
                    <option value="1" selected="selected">Registered</option>
                    <option value="2">Aktif</option>
                    <option value="3">Non Aktif</option>
                    <option value="4">Drop Out</option>
                    <option value="5">Lulus</option>
                    <option value="6">Tidak Lulus</option>
                    <option value="7">Tanpa Keterangan</option>
                    @break
                    @case(2)
                    <option value="1">Registered</option>
                    <option value="2" selected="selected">Aktif</option>
                    <option value="3">Non Aktif</option>
                    <option value="4">Drop Out</option>
                    <option value="5">Lulus</option>
                    <option value="6">Tidak Lulus</option>
                    <option value="7">Tanpa Keterangan</option>
                    @break
                    @case(3)
                    <option value="1">Registered</option>
                    <option value="2">Aktif</option>
                    <option value="3" selected="selected">Non Aktif</option>
                    <option value="4">Drop Out</option>
                    <option value="5">Lulus</option>
                    <option value="6">Tidak Lulus</option>
                    <option value="7">Tanpa Keterangan</option>
                    @break
                    @case(4)
                    <option value="1">Registered</option>
                    <option value="2">Aktif</option>
                    <option value="3">Non Aktif</option>
                    <option value="4" selected="selected">Drop Out</option>
                    <option value="5">Lulus</option>
                    <option value="6">Tidak Lulus</option>
                    <option value="7">Tanpa Keterangan</option>
                    @break
                    @case(5)
                    <option value="1">Registered</option>
                    <option value="2">Aktif</option>
                    <option value="3">Non Aktif</option>
                    <option value="4">Drop Out</option>
                    <option value="5" selected="selected">Lulus</option>
                    <option value="6">Tidak Lulus</option>
                    <option value="7">Tanpa Keterangan</option>
                    @break
                    @case(6)
                    <option value="1">Registered</option>
                    <option value="2">Aktif</option>
                    <option value="3">Non Aktif</option>
                    <option value="4">Drop Out</option>
                    <option value="5">Lulus</option>
                    <option value="6" selected="selected">Tidak Lulus</option>
                    <option value="7">Tanpa Keterangan</option>
                    @break
                    @case(7)
                    <option value="1">Registered</option>
                    <option value="2">Aktif</option>
                    <option value="3">Non Aktif</option>
                    <option value="4">Drop Out</option>
                    <option value="5">Lulus</option>
                    <option value="6">Tidak Lulus</option>
                    <option value="7" selected="selected">Tanpa Keterangan</option>
                    @break
                    @endswitch
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="form-group">
                <label for="tanggal_lahir">Tanggal Lahir</label>
                <input value="{{$userEdit->tanggal_lahir}}" type="text" name="tanggal_lahir" class="form-control"
                    id="tanggal_lahir" required>
            </div>
        </div>
        <div class="col">
            <div class="form-group">
                <label for="gender">Jenis Kelamin</label>
                <select type="text" name="gender" class="form-control" id="gender" required>
                    @switch($userEdit->gender)
                    @case(1)
                    <option value="1" selected="selected">Laki-laki</option>
                    <option value="0">Perempuan</option>
                    @break
                    @case(0)
                    <option value="1">Laki-laki</option>
                    <option value="0" selected="selected">Perempuan</option>
                    @break
                    @default
                    <option value="" selected="selected">Pilih jenis kelamin...</option>
                    @endswitch
                </select>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label for="alamat">Alamat</label>
        <textarea rows="3" name="alamat" class="form-control" id="alamat" required>{{$userEdit->alamat}}</textarea>
    </div>
    <div class="form-group">
        <label for="keterangan">Keterangan</label>
        <textarea rows="3" name="keterangan" class="form-control" id="keterangan"
            required>{{$userEdit->keterangan}}</textarea>
    </div>
    <button type="submit" class="btn btn-primary float-right">Submit</button>
    <button type="button" class="btn btn-outline-primary float-right mr-3" data-dismiss="modal">Batal</button>
</form>
