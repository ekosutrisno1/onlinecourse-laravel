@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col">
                            <h2 class="display-4">
                                User Registered
                                <i class="fa fa-graduation-cap"></i>
                            </h2>
                            <p>
                                Berisi semua Data Peserta course yang disediakan di <span
                                    class="font-weight-bold text-primary">CO-ExoApp</span> telah
                                mengikuti dan memenuhi
                                standar kebutuhan
                                industri,sehinggga lulusan <span class="font-weight-bold text-primary">CO-ExoApp</span>
                                mempunyai
                                pleuang yang besar untuk siap terjun kednuia kerja.
                            </p>
                            <h3>
                                Total Peserta teregistrasi: <span class="badge badge-primary">
                                    {{$data->total()}}
                                    user</span>
                            </h3>
                            <br>
                            <a href="javascript:history.back()" class="btn btn-primary btn-sm">
                                <i class="fa fa-fw fa-arrow-left"></i> Back</a>
                        </div>
                    </div>
                </div>
                <div class="card-body p-3">

                    @if (session('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>Yee Berhasil </strong> {{session('success')}}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif

                    <table class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>Identitas</th>
                                <th>Kelahiran</th>
                                <th>Alamat</th>
                                <th>Keterangan</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $user)
                            <tr>
                                <td>
                                    <p>
                                        <span>
                                            Email : <br>
                                            <span class="font-weight-bold">{{$user->email}}</span>
                                        </span><br>
                                        <span>
                                            No ID : <br>
                                            <span class="font-weight-bold text-danger">{{$user->no_identitas}}</span>
                                        </span><br>
                                    </p>
                                </td>
                                <td>
                                    <p>
                                        <span>
                                            Tempat lahir : <br>
                                            <span class="font-weight-bold">{{$user->tempat_lahir}}</span>
                                        </span><br>
                                        <span>
                                            Tanggal lahir : <br>
                                            <span class="font-weight-bold">{{$user->tanggal_lahir}}</span>
                                        </span><br>
                                        <span>
                                            Jenis Kelamin : <br>
                                            @if ($user->gender == 1)
                                            <span class="font-weight-bold">Laki-laki</span>
                                            @else
                                            <span class="font-weight-bold">Perempuan</span>
                                            @endif
                                        </span><br>
                                    </p>
                                </td>
                                <td>
                                    <span>
                                        Alamat : <br>
                                        <span class="font-weight-bold">{{$user->alamat}}</span>
                                    </span>
                                </td>
                                <td>{{$user->keterangan}}</td>
                                <td>
                                    @switch($user->status)
                                    @case(1)
                                    <span class="font-weight-bold text-success">Registered
                                        <i class="fa fa-fw fa-graduation-cap"></i></span>
                                    @break
                                    @case(2)
                                    <span class="font-weight-bold text-primary">Aktif
                                        <i class="fa fa-fw fa-graduation-cap"></i></span>
                                    @break
                                    @case(3)
                                    <span class="font-weight-bold text-secondary">Non Aktif
                                        <i class="fa fa-fw fa-graduation-cap"></i></span>
                                    @break
                                    @case(4)
                                    <span class="font-weight-bold text-danger">DO
                                        <i class="fa fa-fw fa-graduation-cap"></i></span>
                                    @break
                                    @case(5)
                                    <span class="font-weight-bold text-primary">LULUS
                                        <i class="fa fa-fw fa-graduation-cap"></i></span>
                                    @break
                                    @case(6)
                                    <span class="font-weight-bold text-danger">Tidak Lulus
                                        <i class="fa fa-fw fa-graduation-cap"></i></span>
                                    @break
                                    @case(7)
                                    <span class="font-weight-bold text-danger">Tanpa Keterangan
                                        <i class="fa fa-fw fa-graduation-cap"></i></span>
                                    @break
                                    @endswitch
                                </td>
                                <td>
                                    <div class="dropdown">
                                        <button class="btn btn-sm btn-primary dropdown-toggle" type="button"
                                            id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                            aria-expanded="false">
                                            Aksi
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <a class="dropdown-item" href="#."
                                                onclick="editRegisteredUser({{$user->id}})">Edit</a>
                                            <a class="dropdown-item" href="#."
                                                onclick="deleteRegisteredUser({{$user->id}})">Delete</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <form class="d-none" id="delete-form" action="" method="POST">
                        {{ method_field('DELETE') }}
                        @csrf
                    </form>
                    <hr>
                    <div class="py-2 float-right">
                        {{$data->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
