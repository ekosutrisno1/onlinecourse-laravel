<form method="POST" action="/access/update">
    @csrf
    <input type="hidden" name="id" id="id" value="{{$userData->id}}">
    <input type="hidden" name="role_lama" id="role_lama" value="{{$userData->role}}">
    <div class="form-group">
        <label for="name" class="form-label">{{ __('Name') }}</label>
        <input value="{{$userData->name}}" id="name" type="text"
            class="form-control @error('name') is-invalid @enderror" name="name" value="" required autocomplete="name"
            autofocus>
        @error('name')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

    <div class="form-group">
        <label for="email" class="form-label">{{ __('E-Mail Address') }}</label>
        <input value="{{$userData->email}}" id="email" type="email"
            class="form-control @error('email') is-invalid @enderror" name="email" value="" required
            autocomplete="email">
        @error('email')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

    <div class="form-group">
        <label for="role" class="form-label">{{ __('Plih Access') }}</label>
        <select id="role" type="text" class="form-control @error('role') is-invalid @enderror" name="role" value=""
            required autocomplete="role">
            @foreach ($role as $ro)
            @if ($userData->role===$ro->id)
            <option value="{{$ro->id}}" selected="selected">{{$ro->name}}</option>
            @else
            <option value="{{$ro->id}}">{{$ro->name}}</option>
            @endif
            @endforeach
        </select>
        @error('email')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary float-right">
        {{ __('Perbarui') }} <i class="fa fa-fw fa-plus"></i>
    </button>
</form>
