<form method="POST" action="/access/store">
    @csrf

    <div class="form-group">
        <label for="name" class="form-label">{{ __('Name') }}</label>
        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name"
            value="{{ old('name') }}" required autocomplete="name" autofocus>
        @error('name')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

    <div class="form-group">
        <label for="email" class="form-label">{{ __('E-Mail Address') }}</label>
        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email"
            value="{{ old('email') }}" required autocomplete="email">
        @error('email')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

    <div class="form-group">
        <label for="role" class="form-label">{{ __('Plih Access') }}</label>
        <select id="role" type="text" class="form-control @error('role') is-invalid @enderror" name="role"
            value="{{ old('role') }}" required autocomplete="role">
            @foreach ($role as $ro)
            <option value="{{$ro->id}}">{{$ro->name}}</option>
            @endforeach
        </select>
        @error('email')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

    <div class="form-group d-none">
        <label for="password" class="form-label">{{ __('Password') }}</label>
        <input value="12345678" id="password" type="password"
            class="form-control @error('password') is-invalid @enderror" name="password" required
            autocomplete="new-password">

        @error('password')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

    <div class="form-group d-none">
        <label for="password-confirm" class="form-label">{{ __('Confirm Password') }}</label>
        <input value="12345678" id="password-confirm" type="password" class="form-control" name="password_confirmation"
            required autocomplete="new-password">
    </div>
    <button type="submit" class="btn btn-primary float-right">
        {{ __('Tambahkan') }} <i class="fa fa-fw fa-plus"></i>
    </button>
</form>
