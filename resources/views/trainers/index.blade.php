@extends('layouts.app')

@section('content')
<div class="container">
   <div class="row">
      <h6 class="text-center display-4 ml-3">Access Controll Page <span style="color:#9e0356">
            <i class="fa fa-fw fa-table"></i></span></h6>
      <div class="col">
         @if (session('success'))
         <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>Yee Berhasil </strong> {{session('success')}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
         </div>
         @endif
         @if ($errors->any())
         <div class="alert alert-danger">
            <ul>
               @foreach ($errors->all() as $error)
               <li>{{ $error }}</li>
               @endforeach
            </ul>
         </div>
         @endif
      </div>
   </div>

   <div class="row">
      <div class="col">
         <p>
            Jadwal course yang disediakan di <span class="font-weight-bold text-primary">CO-ExoApp</span> telah
            mengikuti dan memenuhi
            standar kebutuhan
            industri,sehinggga lulusan <span class="font-weight-bold text-primary">CO-ExoApp</span> mempunyai
            pleuang yang besar untuk siap terjun kednuia kerja.
         </p>
         <button class="btn btn-primary" onclick="addAdminOrTrainer()">
            Add Admin / Trainer
            <i class="fa fa-fw fa-plus"></i>
         </button>
      </div>
   </div>
   <hr>
   <div class="row">
      <div class="col-md-7 ">
         <table class="table table-hover">
            <thead>
               <tr>
                  <th>Nama</th>
                  <th>Email</th>
                  <th>Access</th>
                  <th>Ubah</th>
               </tr>
            </thead>
            <tbody>
               @foreach ($user as $i => $u)
               <tr>
                  <td><span class="font-weight-bold">{{$u->name}}</span></td>
                  <td>{{$u->email}}</td>
                  <td>
                     <p>
                        @switch($u->role)
                        @case(1)
                        <span class="font-weight-bold text-success">Super Admin
                           <i class="fa fa-fw fa-user-shield"></i>
                        </span>
                        @break
                        @case(3)
                        <span class="font-weight-bold text-primary">Trainer/Dosen
                           <i class="fa fa-fw fa-user-shield"></i></span>
                        @break
                        @case(4)
                        <span class="font-weight-bold text-danger">Management
                           <i class="fa fa-fw fa-user-shield"></i></span>
                        @break
                        @endswitch
                     </p>
                  </td>
                  <td>
                     @if (Auth::user()->email===$u->email)
                     <button class="btn btn-sm btn-success disabled">
                        Edit <i class="fa fa-fw fa-pencil"></i>
                     </button>
                     @else
                     <button
                        class="{{ Auth::user()->email===$u->email ? 'btn btn-sm btn-success disabled' : 'btn btn-sm btn-success'}}"
                        onclick="editAdminOrTrainer({{$u->id}})">
                        Edit <i class="fa fa-fw fa-pencil"></i>
                     </button>
                     @endif
                  </td>
               </tr>
               @endforeach
            </tbody>
         </table>
         <div class="py-2 float-right">
            {{$user->links()}}
         </div>
      </div>
      <div class="col-md-5">

         <div class="card mx-auto" style="max-width: 25rem;">
            <div class="card-header">
               <h6>
                  Trainer <i class="fa fa-fw fa-chalkboard-teacher"></i>
                  <span class="font-weight-bold text-success float-right">
                     Status: Aktif <i class="fa fa-fw fa-check"></i>
                  </span>
               </h6>
            </div>
            @if (count($trainer)>0)
            <ul class="list-group list-group-flush">
               @foreach ($trainer as $u)
               <li class="list-group-item">
                  <span class="font-weight-bold text-primary">{{$u->name}} </span>

                  <p class="float-right ml-2">
                     <span class="font-weight-bold text-primary float-left"> | </span>
                     @for ($i = 1; $i <= $u->rating; $i++)
                        <i class="fa fa-fw fa-star" style="color: orange"></i>
                        @endfor
                        @for ($i = 1; $i <= (5 - $u->rating); $i++)
                           <i class="fa fa-fw fa-star text-secondary"></i>
                           @endfor
                  </p>
                  <span class="font-weight-bold text-success float-right">
                     | Trainer <i class="fa fa-fw fa-user-shield"></i>
                  </span>
               </li>
               @endforeach
            </ul>
            @else
            <ul class="list-group list-group-flush">
               <li class="list-group-item">
                  <span class="font-weight-bold text-primary">Trainer belum tersedia/ditambahkan </span>
               </li>
            </ul>
            @endif
         </div>
      </div>
   </div>
</div>
@endsection
