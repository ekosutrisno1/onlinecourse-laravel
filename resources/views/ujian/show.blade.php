@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <h3 class="text-center font-weight-bold ml-3">
            <i class="fa fa-fw fa-circle small"></i> Detail Ujian {{$materi->judul}}
            <span style="color:#9e0356"><i class="fa fa-fw fa-search"></i></span>
        </h3>
    </div>

    <div class="row">
        <div class="col">
            <p>
                Jadwal course yang disediakan di <span class="font-weight-bold text-primary">CO-ExoApp</span> telah
                mengikuti dan memenuhi
                standar kebutuhan
                industri,sehinggga lulusan <span class="font-weight-bold text-primary">CO-ExoApp</span> mempunyai
                pleuang yang besar untuk siap terjun kednuia kerja.
            </p>
            <a href="javascript:history.back()" class="btn btn-primary btn-sm">
                <i class="fa fa-fw fa-arrow-left"></i> Back</a>
            <h4 class="float-right">
                <span class="badge badge-success">
                    <i class="fa fa-fw fa-circle small"></i>
                    Peserta: {{Auth::user()->name}}
                </span>
                |
                <span class="badge badge-danger">
                    <i class="fa fa-fw fa-star small"></i>
                    Score: {{count($score)}}
                </span>
                |
                <span class="badge badge-dark">
                    <i class="fa fa-fw fa-id-card"></i> Soal ke {{$ujian->currentPage()}}
                </span>
                |
                <span class="badge badge-primary">
                    <i class="fa fa-fw fa-list-ol"></i> Total {{$ujian->total()}} soal
                </span>
            </h4>
        </div>
    </div>
    <hr>


    <div class="row">
        <div class="col-md">
            @if ($ujian->count() >0)

            @foreach ($ujian as $uji)
            <div class="card mx-4 mb-4 shadow-sm">
                <div class="card-body">
                    <input type="hidden" name="id_soal" id="id-soal-saat-ini" value="{{$uji->id}}">
                    <div class="row">
                        <div class="col-md-4">
                            <img src="/img/reset-psw.svg" width="100%">
                        </div>
                        <div class="col-md-8">
                            <h3>
                                Tema: {{$materi->judul}}
                            </h3>
                            <p class="lead font-weight-bold">
                                Trainer :
                                <span class="text-primary">{{$trainer->name}}
                                    <i class="fa fa-fw fa-user-shield"></i>
                                </span>
                            </p>
                            <p class="font-weight-bold">Tanggal {{$uji->tanggal}}</p>
                            <p>
                                <span class="font-weight-bold">Pertanyaan:</span> <br>
                                {{$uji->pertanyaan}}
                            </p>
                            <h5>
                                Jenis Ujian :
                                @if ($uji->status == 1)
                                <span class="font-weight-bold text-success">Harian
                                    <i class="fa fa-fw fa-calendar-check"></i>
                                </span>
                                @elseif($uji->status == 0)
                                <span class="font-weight-bold text-danger">Mingguan
                                    <i class="fa fa-fw fa-list"></i></span>
                                @else
                                <span class="font-weight-bold text-primary">Akhir Tema
                                    <i class="fa fa-fw fa-graduation-cap"></i></span>
                                @endif
                            </h5>
                            <button class="btn btn-primary float-right" onclick="jawabSoal()">
                                Jawab Soal <i class="fa fa-fw fa-pencil"></i>
                            </button>
                            <hr>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            @else
            <div class="col-md">
                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                    <strong>Jadwal ujian <span id="status-u"></span> untuk tema {{$materi->judul}} belum
                        tersedia.</strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
            <div class="d-flex align-content-center">
                <img class="align-self-auto" src="/img/no-data.svg" width="300px">
            </div>

            @endif

            <div class="py-2 float-right">
                {{$ujian->links()}}
            </div>
        </div>
    </div>
    <hr>
</div>
</div>


<!-- Modal -->
<div class="modal fade" id="modal-jawaban" tabindex="-1" role="dialog" aria-labelledby="modal-appLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-appLabel">Soal {{$ujian->currentPage()}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="data-form-content">
                <form action="/jawaban_user" method="POST">
                    @csrf
                    <div class="form-group d-none">
                        <input type="hidden" name="id_soal" class="form-control" id="id-soal">
                        <input type="hidden" value="{{$status}}" name="status_soal" class="form-control"
                            id="status_soal">
                        <input type="hidden" value="{{$ujian->currentPage()}}" name="nomor_soal" class="form-control"
                            id="nomor_soal">
                        <input type="hidden" value="{{Auth::user()->id}}" name="peserta_id" class="form-control"
                            id="peserta_id">
                    </div>
                    <div class="form-group d-none">
                        <input value="{{$materi->id}}" type="hidden" name="materi_id" class="form-control"
                            id="materi_id">
                    </div>
                    <div class="form-group">
                        <label for="jawaban">Jawaban</label>
                        <textarea placeholder="Tulis Jawaban..." rows="8" name="jawaban" class="form-control"
                            id="jawaban" required></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary float-right ml-2">
                        Jawab <i class="fa fa-envelope"></i>
                    </button>
                    <button type="reset" class="btn btn-outline-primary float-right">
                        Hapus <i class="fa fa-trash"></i>
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
