<form action="/file-data/import" method="POST" enctype="multipart/form-data">
    <h4>File: <strong>
            <span class="text-success">xls</span>
            <span class="text-warning">xlsx</span>
            <span class="text-primary">csv</span>
        </strong></h4>
    @csrf
    <div class="form-group">
        <label for="file">Pilih File</label>
        <input class="form-control-file" type="file" name="file" id="file" required>

    </div>
    <p class="float-right">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Import <i class="fa fa-upload"></i> </button>
    </p>
</form>
