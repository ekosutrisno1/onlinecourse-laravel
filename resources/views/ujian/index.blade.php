@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <h6 class="text-center display-4 ml-2">Ujian Page <span style=" color:#052b94">
                <i class="fa fa-fw fa-tasks"></i></span></h6>
        <div class="col">
            @if (session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>Yee Berhasil </strong> {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif
        </div>
    </div>

    <div class="row">
        <div class="col">
            <p>
                Jadwal course yang disediakan di <span class="font-weight-bold text-primary">CO-ExoApp</span> telah
                mengikuti dan memenuhi
                standar kebutuhan
                industri,sehinggga lulusan <span class="font-weight-bold text-primary">CO-ExoApp</span> mempunyai
                pleuang yang besar untuk siap terjun kednuia kerja. Total soal tersedia
                <span class="badge badge-danger">{{$ujian->total()}}</span> Soal.
            </p>
            <p>
                <button class="btn btn-primary mb-2" onclick="addUjian()">Add Soal Ujian Tema
                    <i class="fa fa-fw fa-plus"></i></button>
                <button class="btn btn-success mb-2" onclick="importUjian()">Import dari Excel
                    <i class="fa fa-fw fa-upload"></i></button>
                <button class="btn btn-success mb-2" onclick="exportUjian()">Export ke Excel
                    <i class="fa fa-fw fa-download"></i></button>
            </p>
            <p>

                <form action="/search/data" method="POST">
                    @csrf
                    <input type="hidden" name="status" value="2">
                    <button class="btn btn-primary mt-2 btn-sm float-right mr-2" type="submit">
                        Akhir Tema <i class="fa fa-fw fa-graduation-cap"></i></button>
                </form>
                <form action="/search/data" method="POST">
                    @csrf
                    <input type="hidden" name="status" value="0">
                    <button class="btn btn-primary mt-2 btn-sm float-right mr-2" type="submit">
                        Mingguan <i class="fa fa-fw fa-list"></i></button>
                </form>
                <form action="/search/data" method="POST">
                    @csrf
                    <input type="hidden" name="status" value="1">
                    <button class="btn btn-primary mt-2 btn-sm float-right mr-2" type="submit">
                        Harian <i class="fa fa-fw fa-calendar-check"></i></button>
                </form>
                <a href="/ujian" class="btn btn-success mt-2 btn-sm float-right mr-2">
                    All <i class="fa fa-fw fa-align-left"></i> </a>
                <span class="font-weight-bold float-right mr-3">Cari Berdasarkan: </span>
            </p>
        </div>
    </div>
    <hr>

    @if (count($ujian)> 0)
    <div class="row">
        <div class="d-flex justify-content-around flex-wrap">
            @foreach ($ujian as $i => $uji)
            <div class="accordion mx-3 mb-2" id="accordionUjian">
                <div class="card" style="width: 27.5rem">
                    <div class="card-header" id="heading{{$i}}">
                        <p class="mb-0">
                            <button class="btn btn-sm btn-outline-dark" type="button" data-toggle="collapse"
                                data-target="#collapse{{$i}}" aria-expanded="true" aria-controls="collapse{{$i}}">
                                <span class="font-weight-bold">Halaman {{$ujian->currentPage()}}</span> | Soal
                                {{$i + 1}}
                            </button>
                            |
                            @if ($uji->status == 1)
                            <span class="font-weight-bold text-success">Harian <i
                                    class="fa fa-fw fa-calendar-check"></i></span>
                            @elseif($uji->status == 0)
                            <span class="font-weight-bold text-danger">Mingguan <i class="fa fa-fw fa-list"></i></span>
                            @else
                            <span class="font-weight-bold text-primary">Akhir Tema
                                <i class="fa fa-fw fa-graduation-cap"></i></span>
                            @endif
                        </p>
                    </div>

                    <div id="collapse{{$i}}" class="collapse" aria-labelledby="heading{{$i}}"
                        data-parent="#accordionUjian">
                        <div class="card-body">
                            <div class="card shadow-sm zoom-card-ujian" style="width:25rem;">
                                <div class="card-body">
                                    <h3 class="card-title">{{$uji->judul}} |
                                        @if ($uji->status == 1)
                                        <span class="font-weight-bold text-success">Harian <i
                                                class="fa fa-fw fa-calendar-check"></i></span>
                                        @elseif($uji->status == 0)
                                        <span class="font-weight-bold text-danger">Mingguan <i
                                                class="fa fa-fw fa-list"></i></span>
                                        @else
                                        <span class="font-weight-bold text-primary">Akhir Tema
                                            <i class="fa fa-fw fa-graduation-cap"></i></span>
                                        @endif
                                    </h3>

                                    <p class="card-text">
                                        <span class="font-weight-bold text-primary">
                                            Pertanyaan <i class="fa fa-question"></i></span>
                                        <br>
                                        {{$uji->pertanyaan}}
                                        <hr>
                                        <span class="font-weight-bold text-success">
                                            Jawaban <i class="fa fa-check"></i></span>
                                        <br>
                                        {{$uji->jawaban}}
                                    </p>

                                    <p class="font-weight-bold">Tanggal {{$uji->tanggal}} <i
                                            class="fa fa-fw fa-calendar-alt text-success"></i></p>

                                    <p>
                                        <button onclick="editUjian({{$uji->id}})" class="btn btn-primary btn-sm">Edit
                                            <i class="fa fa-fw fa-edit"></i></button>
                                        <button type="button" class="btn btn-primary btn-sm"
                                            onclick="deleteUjian({{$uji->id}})">Hapus
                                            <i class="fa fa-fw fa-trash"></i></button>
                                    </p>
                                </div>
                            </div>
                            <form class="d-none" id="delete-form" action="" method="POST">
                                {{ method_field('DELETE') }}
                                @csrf
                            </form>

                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    @else
    <div class="col-md">
        <div class="alert alert-primary alert-dismissible fade show" role="alert">
            <strong>Tidak ada Ujian yang tersedia.</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
    <div class="d-flex align-content-center">
        <img class="align-self-auto" src="/img/no-data.svg" width="300px">
    </div>

    @endif
    <div class="py-2 float-right">
        {{$ujian->links()}}
    </div>
</div>
@endsection
