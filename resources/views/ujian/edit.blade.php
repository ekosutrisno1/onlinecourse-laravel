<form action="/ujian/{{$ujian->id}}" method="POST" enctype="multipart/form-data">
    @csrf
    <input type="hidden" name="id" value="{{$ujian->id}}">
    {{ method_field('PUT') }}
    <div class="form-group">
        <label for="tanggal">Tanggal ujian</label>
        <input value="{{$ujian->tanggal}}" type="text" name="tanggal" class="form-control" id="tanggal">
    </div>
    <div class="row">
        <div class="col-md">
            <div class="form-group">
                <label for="materi_id">Pilih Tema ujian</label>
                <select type="text" name="materi_id" class="form-control" id="materi_id">
                    <option value="" selected="selected">Pilih Tema...</option>
                    @foreach ($materi as $mat)
                    @if ($ujian->materi_id == $mat->id)
                    <option value="{{$mat->id}}" selected="selected">{{$mat->judul}}</option>
                    @endif
                    <option value="{{$mat->id}}">{{$mat->judul}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md">
            <div class="form-group">
                <label for="status">Status ujian</label>
                <select type="text" name="status" class="form-control" id="status">
                    @switch($ujian->status)
                    @case(1)
                    <option value="1" selected="selected">Harian</option>
                    <option value="0">Mingguan</option>
                    <option value="2">Akhir Tema</option>
                    @break
                    @case(0)
                    <option value="1">Harian</option>
                    <option value="0" selected="selected">Mingguan</option>
                    <option value="2">Akhir Tema</option>
                    @break
                    @case(2)
                    <option value="1">Harian</option>
                    <option value="0">Mingguan</option>
                    <option value="2" selected="selected">Akhir Tema</option>
                    @break
                    @endswitch

                </select>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="pertanyaan">Pertanyaan</label>
        <textarea placeholder="Tulis Pertanyaan..." maxlength="300" rows="4" name="pertanyaan" class="form-control"
            id="pertanyaan">{{$ujian->pertanyaan}}</textarea>
    </div>
    <div class="form-group">
        <label for="jawaban">Jawaban</label>
        <textarea placeholder="Tulis Jawaban..." maxlength="300" rows="4" name="jawaban" class="form-control"
            id="jawaban">{{$ujian->jawaban}}</textarea>
    </div>
    <button type="submit" class="btn btn-primary float-right ml-2">Submit</button>
    <button type="reset" class="btn btn-outline-primary float-right">Reset</button>
</form>
