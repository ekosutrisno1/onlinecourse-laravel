@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <h6 class="text-center display-4 ml-3">Jadwal Page <span style="color:#9e0356">
                <i class="fa fa-fw fa-calendar"></i></span></h6>
        <div class="col">
            @if (session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>Yee Berhasil </strong> {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif
        </div>
    </div>

    <div class="row">
        <div class="col">
            <p>
                Jadwal course yang disediakan di <span class="font-weight-bold text-primary">CO-ExoApp</span> telah
                mengikuti dan memenuhi
                standar kebutuhan
                industri,sehinggga lulusan <span class="font-weight-bold text-primary">CO-ExoApp</span> mempunyai
                pleuang yang besar untuk siap terjun kednuia kerja.
            </p>
            <button
                class="{{ (Gate::check('isAdmin')||Gate::check('isDosen')||Gate::check('isManager')) ? 'btn btn-primary' : 'btn btn-primary d-none' }}"
                onclick="addJadwal()">Add
                Jadwal Course
                <i class="fa fa-fw fa-plus"></i>
            </button>
        </div>
    </div>
    <hr>

    @if (count($jadwal)> 0)
    <div class="row">
        <input type="hidden" name="jd_id" id="jd_id">
        <div class="d-flex justify-content-center flex-wrap">
            @foreach ($jadwal as $i => $jdw)
            <div class="card mx-4 mb-4 shadow-sm zoom-card" style="width: 18rem;">
                <img src="/img/bg01.svg" class="card-img-top" alt="...">
                <div class="card-body">
                    <h3 class="card-title">{{$jdw->kode_jadwal}}</h3>
                    <p class="font-weight-bold">Start At {{$jdw->start_date}} <br> End At {{$jdw->end_date}} <br>
                        <span class="font-weight-bold text-success">
                            Trainer |
                        </span>
                        <span class="font-weight-bold text-primary">
                            {{$jdw->name}} <i class="fa fa-fw fa-user-shield"></i>
                        </span>
                    </p>
                    <p>
                        <a href="/jadwal/{{$jdw->id}}" class="btn btn-primary btn-sm">
                            Cekout <i class="fa fa-fw fa-list"></i></a>
                        @if (Gate::check('isAdmin')||Gate::check('isDosen')||Gate::check('isManager'))
                        <button onclick="editJadwal({{$jdw->id}})" class="btn btn-primary btn-sm">Edit
                            <i class="fa fa-fw fa-edit"></i></button>
                        <button type="button" class="btn btn-primary btn-sm" onclick="deleteJadwal({{$jdw->id}})">Hapus
                            <i class="fa fa-fw fa-trash"></i></button>
                        @else
                        <button onclick="ambilJadwalUser({{$jdw->id}})" class="btn btn-primary btn-sm">
                            Ambil jadwal <i class="fa fa-fw fa-check"></i></button>
                        @endif
                    </p>
                </div>
            </div>
            @endforeach
            <form class="d-none" id="delete-form" action="" method="POST">
                {{ method_field('DELETE') }}
                @csrf
            </form>
        </div>
    </div>
    @else
    <div class="alert alert-primary alert-dismissible fade show" role="alert">
        <strong>Tidak ada jadwal yang tersedia.</strong>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="d-flex align-content-center">
        <img class="align-self-auto" src="/img/no-data.svg" width="300px">
    </div>
    @endif
</div>
@endsection
