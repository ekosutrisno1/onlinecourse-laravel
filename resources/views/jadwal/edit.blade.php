<form action="/jadwal/{{$jadwal->id}}" method="POST">
    <input type="hidden" name="id" value="{{$jadwal->id}}">
    {{ method_field('PUT') }}
    @csrf
    <div class="form-group">
        <label for="kode_jadwal">Kode Jadwal</label>
        <input value="{{$jadwal->kode_jadwal}}" type="text" name="kode_jadwal"
            onkeyup="this.value = this.value.toUpperCase();" class="form-control" id="kode_jadwal" placeholder="Kode..."
            maxlength="5">
    </div>
    <div class="form-group">
        <label for="trainer_id" class="form-label">{{ __('Plih Trainer') }}</label>
        <select id="trainer_id" type="text" class="form-control" name="trainer_id" required autocomplete="trainer_id">
            @foreach ($trainer as $ro)
            @if ($jadwal->trainer_id===$ro->id)
            <option value="{{$ro->id}}" selected="selected">{{$ro->name}}</option>
            @else
            <option value="{{$ro->id}}">{{$ro->name}}</option>
            @endif
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="start_date">Tanggal Mulai</label>
        <input value="{{$jadwal->start_date}}" type="text" name="start_date" class="form-control" id="start_date"
            placeholder="Start date...">
    </div>
    <div class="form-group">
        <label for="end_date">Tanggal Selesai</label>
        <input value="{{$jadwal->end_date}}" type="text" name="end_date" class="form-control" id="end_date"
            placeholder="End date...">
    </div>
    <button type="submit" class="btn btn-primary float-right">Submit</button>
</form>
