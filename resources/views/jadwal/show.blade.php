@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <h3 class="text-center font-weight-bold ml-3">Materi untuk Jadwal {{$jadwal->kode_jadwal}}
            <span style="color:#9e0356"><i class="fa fa-fw fa-calendar"></i></span></h3>
    </div>

    <div class="row">
        <div class="col">
            <p>
                Jadwal course yang disediakan di <span class="font-weight-bold text-primary">CO-ExoApp</span> telah
                mengikuti dan memenuhi
                standar kebutuhan
                industri,sehinggga lulusan <span class="font-weight-bold text-primary">CO-ExoApp</span> mempunyai
                pleuang yang besar untuk siap terjun kednuia kerja.
            </p>
            <a href="javascript:history.back()" class="btn btn-primary"><i class="fa fa-fw fa-arrow-left">
                </i> Back to Jadwal</a>
        </div>
    </div>
    <hr>

    @if (count($materi)> 0)
    <div class="row">
        <div class="d-flex justify-content-center flex-wrap">
            @foreach ($materi as $i => $mat)
            <div class="card mx-4 mb-4 shadow-sm zoom-card-jadwal-show" style="width: 18rem;">
                <img src="/img/cap2.svg" class="card-img-top" alt="...">
                <div class="card-body">
                    <h3 class="card-title">{{$mat->judul}}</h3>
                    <p class="lead font-weight-bold">
                        <span class="font-weight-bold text-success">
                            Trainer |
                        </span>
                        <span class="font-weight-bold text-primary">
                            {{$mat->name}} <i class="fa fa-fw fa-user-shield"></i>
                        </span>
                    </p>
                    <p class="font-weight-bold">Tanggal {{$mat->tanggal}}</p>
                    <h5>@if ($mat->status == 1)
                        <span class="font-weight-bold text-success">Open <i class="fa fa-fw fa-folder-open"></i></span>
                        @else
                        <span class="font-weight-bold text-danger">Close <i class="fa fa-fw fa-folder"></i></span>
                        @endif
                    </h5>
                    <hr>
                    <p class="{{ Gate::check('isAdmin') ? '' : 'd-none' }}">
                        <a href="/materi/{{$mat->id}}" class="btn btn-primary btn-sm">Detail Materi
                            <i class="fa fa-fw fa-list"></i></a>
                    </p>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    @else

    <div class="col-md">
        <div class="alert alert-primary alert-dismissible fade show" role="alert">
            <strong>Tidak ada materi yang tersedia untuk jadwal {{$jadwal->kode_jadwal}}.</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
    <div class="d-flex align-content-center">
        <img class="align-self-auto" src="/img/no-data.svg" width="300px">
    </div>

    @endif
</div>
@endsection
