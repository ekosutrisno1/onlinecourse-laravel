<div class="card text-center">
    <div class="card-header">
        Information
    </div>
    <div class="card-body">
        <h5 class="card-title">Hello {{Auth::user()->name}}</h5>
        <p class="card-text">Selamat datang di OC-ExoApp, Kamu dapat melakukan pemilihan jadwal sesuai keinginan dan
            waktu.</p>
        <a href="/jadwal" class="btn btn-primary">
            Ambil Jadwal <i class="fa fa-fw fa-calendar"></i>
        </a>
    </div>
</div>
