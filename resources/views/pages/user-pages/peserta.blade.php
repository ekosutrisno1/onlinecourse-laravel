<div class="card mb-3">
    <div class="card-header">
        <p class="lead">Jadwal saat ini <i class="fa fa-fw fa-swatchbook"></i></p>
        @if ($jadwal)
        <h4 class="card-title text-primary font-weight-bold">
            {{$jadwal->kode_jadwal}} <i class="fa fa-fw fa-bookmark"></i>
        </h4>
        @else
        <h4 class="card-title text-primary font-weight-bold">
            Tidak ada jadwal <i class="fa fa-fw fa-bookmark"></i>
        </h4>
        @endif
    </div>
</div>
<div class="card">
    <div class="card-header">
        Info <i class="fa fa-fw fa-info"></i>
    </div>
    <p class="ml-3 mt-3"><span class="font-weight-bold">Tema Terambil</span>
        <br> Klik tombol ujian untuk mengambil evaluasi.</p>
    <div class="card-body">
        @if ($materi)
        @foreach ($materi as $mat)

        <div class="card p-3 mb-1 zoom-card-materi">
            <p class="lead font-weight-bold">
                <i class="fa fa-check"></i> {{$mat->judul}}
            </p>
            <p class="card-text">
                <span class="font-weight-bold">Sinopsis: </span>
                {{Str::limit($mat->sinopsis, 100,'...')}}
            </p>

            <p>
                <a class="{{ ($infoUser->status == 1) ? 'btn btn-sm btn-outline-success mb-2 disabled' : 'btn btn-sm btn-outline-success mb-2' }}"
                    style="text-decoration: none" href="/materi/{{$mat->id}}">
                    Detail Tema <i class="fa fa-fw fa-search-plus"></i>
                </a>
                <br>
                <span class="font-weight-bold mb-2">Ujian: </span>
                <br>
                <a class="{{ ($infoUser->status == 1) ? 'btn btn-sm btn-outline-primary mb-2 disabled' : 'btn btn-sm btn-outline-primary mb-2' }}"
                    style="text-decoration: none" href="payment/ujian/{{$mat->id}}/1">
                    Harian
                </a>
                <a class="{{ ($infoUser->status == 1) ? 'btn btn-sm btn-outline-primary mb-2 disabled' : 'btn btn-sm btn-outline-primary mb-2' }}"
                    style="text-decoration: none" href="payment/ujian/{{$mat->id}}/0">
                    Mingguan
                </a>
                <a class="{{ ($infoUser->status == 1) ? 'btn btn-sm btn-outline-danger mb-2 disabled' : 'btn btn-sm btn-outline-danger mb-2' }}"
                    style="text-decoration: none" href="payment/ujian/{{$mat->id}}/2">
                    Akhir Tema
                </a>
            </p>
        </div>
        @endforeach
        @else
        <p class="lead font-weight-bold">
            <i class="fa fa-check"></i> Kosong
        </p>
        @endif
    </div>
</div>
