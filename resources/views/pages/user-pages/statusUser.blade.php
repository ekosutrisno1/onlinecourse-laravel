@if ($jadwal)
<small class="text-primary font-weight-bold">| ID:
    {{$infoUser->no_identitas}}</small>

<br>
Status :
@switch($infoUser->status)
@case(1)
<span class="font-weight-bold text-success">Registered
    <i class="fa fa-fw fa-graduation-cap"></i></span>
@break
@case(2)
<span class="font-weight-bold text-primary">Aktif
    <i class="fa fa-fw fa-graduation-cap"></i></span>
@break
@case(3)
<span class="font-weight-bold text-secondary">Non Aktif
    <i class="fa fa-fw fa-graduation-cap"></i></span>
@break
@case(4)
<span class="font-weight-bold text-danger">DO
    <i class="fa fa-fw fa-graduation-cap"></i></span>
@break
@case(5)
<span class="font-weight-bold text-primary">LULUS
    <i class="fa fa-fw fa-graduation-cap"></i></span>
@break
@case(6)
<span class="font-weight-bold text-danger">Tidak Lulus
    <i class="fa fa-fw fa-graduation-cap"></i></span>
@break
@case(7)
<span class="font-weight-bold text-danger">Tanpa Keterangan
    <i class="fa fa-fw fa-graduation-cap"></i></span>
@break
@endswitch
@endif
