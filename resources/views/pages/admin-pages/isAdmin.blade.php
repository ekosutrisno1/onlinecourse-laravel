<div class="card text-white bg-primary mb-3 mx-auto" style="max-width: 18rem;">
    <div class="card-header">Jumlah Peserta <i class=" fa fa-user"></i></div>
    <div class="card-body">
        <h5 class="card-title">Statistik Peserta Terdaftar</h5>
        <h3 class="font-weight-bold">{{$peserta}} Peserta </h3>
    </div>
</div>
<div class="card text-white bg-secondary mb-3 mx-auto" style="max-width: 18rem;">
    <div class="card-header">Jawaban Peserta <i class="fa fa-question"></i></div>
    <div class="card-body">
        <h5 class="card-title">Statistik Soal Terjawab</h5>
        <h3 class="font-weight-bold">{{$jawaban}} Soal</h3>
    </div>
</div>
<div class="card text-white bg-success mb-3 mx-auto" style="max-width: 18rem;">
    <div class="card-header">Bank Soal Tersedia <i class="fa fa-pencil"></i></div>
    <div class="card-body">
        <h5 class="card-title">Statistik Bank Soal</h5>
        <h6 class="font-weight-bold">
            H: {{$soalHarian}} Soal |
            M: {{$soalMingguan}} Soal |
            A: {{$soalAkhir}} Soal |
        </h6>
    </div>
</div>
