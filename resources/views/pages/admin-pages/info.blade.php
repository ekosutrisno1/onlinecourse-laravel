<div class="card text-center">
    <div class="card-header">
        Information
    </div>
    <div class="card-body">
        <h5 class="card-title">Hello {{Auth::user()->name}}</h5>
        <p class="card-text">Selamat datang di OC-ExoApp, Kamu dapat melakukan pengelolaan semua sumber daya pada
            aplikasi ExoApp.</p>
        <a href="/admin" class="btn btn-primary">
            Maintenents <i class="fa fa-fw fa-gear"></i>
        </a>
    </div>
</div>
