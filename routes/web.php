<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify' => true]);

Route::group(['middleware' => ['verified']], function () {

    Route::get('dashboard', 'DashboardController@index');
    Route::get('unauthorize', 'AdminController@errorHandle');

    Route::group(['middleware' => ['admin']], function () {
        Route::get('admin', 'AdminController@index');
    });

    //Controller
    Route::resource('admin_user', 'AdminUserController');
    Route::resource('registered_user', 'RegisteredUserController');
    Route::resource('jadwal', 'JadwalController');
    Route::resource('jadwal_user', 'JadwalUserController');
    Route::resource('materi', 'MateriController');
    Route::resource('ujian', 'UjianController');
    Route::resource('jawaban_user', 'JawabanUserController');

    Route::get('file-data', 'ExportImportController@getImport');
    Route::get('file-data/export', 'ExportImportController@exportFile');
    Route::post('file-data/import', 'ExportImportController@importFile');

    Route::get('payment/{id}', 'PaymentController@index');
    Route::post('payment/transaksi', 'PaymentController@bayarCourse');
    Route::get('payment/ujian/{id}/{status}', 'PaymentController@getUjianByStatus');
    Route::post('search/data', 'PaymentController@searchUjianByStatus');

    Route::get('access', 'UserAccessController@index');
    Route::get('access/create', 'UserAccessController@create');
    Route::get('access/{id}', 'UserAccessController@edit');
    Route::post('access/store', 'UserAccessController@store');
    Route::post('access/update', 'UserAccessController@update');

    Route::get('/certificate', 'CertificateController@downloadPdf');
    Route::get('/certificate/index', 'CertificateController@index');

});
