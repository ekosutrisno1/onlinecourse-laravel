//Custom Swal notifikasi untuk confirmasi hapus
const cSwal = Swal.mixin({
    customClass: {
        confirmButton: 'btn btn-primary btn-sm mr-4',
        cancelButton: 'btn btn-danger btn-sm'
    },
    buttonsStyling: false
})


// Semua fungsi untuk transaksi dilakuakn disini

function addJadwal() {
    $.ajax({
        url: `/jadwal/create`,
        type: 'get',
        contentType: 'html',
        success: function (res) {
            $('#modal-app').modal('show');
            $('.modal-title').text('ADD JADWAL COURSE');
            $('#data-form-content').html(res);
            showDateJadwal();

        }
    })
}

function editJadwal(id) {
    $.ajax({
        url: `/jadwal/${id}/edit`,
        type: 'get',
        contentType: 'html',
        success: function (res) {
            $('#modal-app').modal('show');
            $('.modal-title').text('EDIT JADWAL COURSE');
            $('#data-form-content').html(res);
            showDateJadwal();

        }
    })
}

function deleteJadwal(id) {
    document.getElementById("delete-form").action = `/jadwal/${id}`;
    cSwal.fire({
        text: 'Akan menghapus jadwal?',
        icon: 'question',
        width: 400,
        padding: '2em',
        background: '#fff url(/img/swal.png)',
        showCancelButton: true,
        confirmButtonColor: '#052b94',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Hapus',
        cancelButtonText: 'Batal'
    }).then((result) => {
        if (result.value) {
            document.getElementById('delete-form').submit();
            Swal.fire({
                position: 'bottom-end',
                icon: 'success',
                title: 'Tema telah dihapus.',
                showConfirmButton: false,
                width: 400,
                padding: '2em',
                background: '#fff url(/img/swal.png)'
            })
        }
    })
}

function showDateJadwal() {
    $("#start_date, #end_date").daterangepicker({
        autoUpdateInput: true,
        singleDatePicker: true,
        minYear: 1980,
        maxYear: parseInt(moment().format("YYYY"), 10),
        locale: {
            format: "YYYY-MM-DD"
        }
    });
}

function ambilJadwalUser(id) {
    $('#jd_id').val(id);
    let jadwal_id = $('#jd_id').val()

    $.ajax({
        url: `/registered_user/create`,
        type: 'get',
        contentType: 'html',
        success: function (res) {
            $('#modal-app').modal('show');
            $('.modal-title').text('Identitas Peserta');
            $('#data-form-content').html(res);
            $('#jadwal_id').val(jadwal_id);
            showDateUser();
        }
    })
}



// Section untuk Materi ***************

function addMateri() {
    $.ajax({
        url: `/materi/create`,
        type: 'get',
        contentType: 'html',
        success: function (res) {
            $('#modal-app').modal('show');
            $('.modal-title').text('ADD JADWAL COURSE');
            $('#data-form-content').html(res);
            showDateMateri();

        }
    })
}

function editMateri(id) {
    $.ajax({
        url: `/materi/${id}/edit`,
        type: 'get',
        contentType: 'html',
        success: function (res) {
            $('#modal-app').modal('show');
            $('.modal-title').text('EDIT JADWAL COURSE');
            $('#data-form-content').html(res);
            showDateMateri();

        }
    })
}

function deleteMateri(id) {
    document.getElementById("delete-form").action = `/materi/${id}`;
    cSwal.fire({
        text: 'Akan menghapus tema?',
        icon: 'question',
        width: 400,
        padding: '2em',
        background: '#fff url(/img/swal.png)',
        showCancelButton: true,
        confirmButtonColor: '#052b94',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Hapus',
        cancelButtonText: 'Batal'
    }).then((result) => {
        if (result.value) {
            document.getElementById('delete-form').submit()
            Swal.fire({
                position: 'bottom-end',
                icon: 'success',
                title: 'Tema telah dihapus.',
                showConfirmButton: false,
                width: 400,
                padding: '2em',
                background: '#fff url(/img/swal.png)'
            })
        }
    })
}

function showDateMateri() {
    $("#tanggal").daterangepicker({
        autoUpdateInput: true,
        singleDatePicker: true,
        minYear: 1980,
        maxYear: parseInt(moment().format("YYYY"), 10),
        locale: {
            format: "YYYY-MM-DD"
        }
    });
}

// Section untuk Ujian ***************
function addUjian() {
    $.ajax({
        url: `/ujian/create`,
        type: 'get',
        contentType: 'html',
        success: function (res) {
            $('#modal-app').modal('show');
            $('.modal-title').text('ADD UJIAN TEMA');
            $('#data-form-content').html(res);
            showDateUjian();

        }
    })
}

function editUjian(id) {
    $.ajax({
        url: `/ujian/${id}/edit`,
        type: 'get',
        contentType: 'html',
        success: function (res) {
            $('#modal-app').modal('show');
            $('.modal-title').text('EDIT UJIAN TEMA');
            $('#data-form-content').html(res);
            showDateMateri();

        }
    })
}

function deleteUjian(id) {
    document.getElementById("delete-form").action = `/ujian/${id}`;
    cSwal.fire({
        text: 'Akan menghapus tema?',
        icon: 'question',
        width: 400,
        padding: '2em',
        background: '#fff url(/img/swal.png)',
        showCancelButton: true,
        confirmButtonColor: '#052b94',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Hapus',
        cancelButtonText: 'Batal'
    }).then((result) => {
        if (result.value) {
            document.getElementById('delete-form').submit()
            Swal.fire({
                position: 'bottom-end',
                icon: 'success',
                title: 'Tema telah dihapus.',
                showConfirmButton: false,
                width: 400,
                padding: '2em',
                background: '#fff url(/img/swal.png)'
            })
        }
    })
}

function showDateUjian() {
    $("#tanggal").daterangepicker({
        autoUpdateInput: true,
        singleDatePicker: true,
        minYear: 1980,
        maxYear: parseInt(moment().format("YYYY"), 10),
        locale: {
            format: "YYYY-MM-DD"
        }
    });
}

function jawabSoal() {
    let idSoal = $('#id-soal-saat-ini').val();
    $('#id-soal').val(idSoal);
    $('#modal-jawaban').modal('show');
}

function importUjian() {
    $.ajax({
        url: `/file-data`,
        type: 'get',
        contentType: 'html',
        success: function (res) {
            $('#modal-app').modal('show');
            $('.modal-title').text('Import File');
            $('#data-form-content').html(res);
        }
    })
}

function exportUjian() {
    location.replace('/file-data/export');
}


//Jawaban user section
function deleteJawaban(id) {
    document.getElementById("delete-form").action = `/jawaban_user/${id}`;
    cSwal.fire({
        text: 'Akan menghapus jawaban?',
        icon: 'question',
        width: 400,
        padding: '2em',
        background: '#fff url(/img/swal.png)',
        showCancelButton: true,
        confirmButtonColor: '#052b94',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Hapus',
        cancelButtonText: 'Batal'
    }).then((result) => {
        if (result.value) {
            document.getElementById('delete-form').submit()
            Swal.fire({
                position: 'bottom-end',
                icon: 'success',
                title: 'Jawaban telah dihapus.',
                showConfirmButton: false,
                width: 400,
                padding: '2em',
                background: '#fff url(/img/swal.png)'
            })
        }
    })
}

//Registered user section
function deleteRegisteredUser(id) {
    document.getElementById("delete-form").action = `/registered_user/${id}`;
    cSwal.fire({
        text: 'Akan menghapus Peserta?',
        icon: 'question',
        width: 400,
        padding: '2em',
        background: '#fff url(/img/swal.png)',
        showCancelButton: true,
        confirmButtonColor: '#052b94',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Hapus',
        cancelButtonText: 'Batal'
    }).then((result) => {
        if (result.value) {
            document.getElementById('delete-form').submit()
            Swal.fire({
                position: 'bottom-end',
                icon: 'success',
                title: 'Peserta telah dihapus.',
                showConfirmButton: false,
                width: 400,
                padding: '2em',
                background: '#fff url(/img/swal.png)'
            })
        }
    })
}

function editRegisteredUser(id) {
    $.ajax({
        url: `/registered_user/${id}/edit`,
        type: 'get',
        contentType: 'html',
        success: function (res) {
            $('#modal-app').modal('show');
            $('.modal-title').text('EDIT PESERTA KURSUS');
            $('#data-form-content').html(res);
            showDateUser();
        }
    })
}

function showDateUser() {
    $("#tanggal_lahir").daterangepicker({
        autoUpdateInput: true,
        singleDatePicker: true,
        showDropdowns: true,
        minYear: 1980,
        maxYear: parseInt(moment().format("YYYY"), 10),
        locale: {
            format: "YYYY-MM-DD"
        }
    });
}

//trainer section
function addAdminOrTrainer() {
    $.ajax({
        url: `/access/create`,
        type: 'get',
        contentType: 'html',
        success: function (res) {
            $('#modal-app').modal('show');
            $('.modal-title').text('TAMBAH ADMIN/TRAINER');
            $('#data-form-content').html(res);
        }
    })
}

function editAdminOrTrainer(id) {
    $.ajax({
        url: `/access/${id}`,
        type: 'get',
        contentType: 'html',
        success: function (res) {
            $('#modal-app').modal('show');
            $('.modal-title').text('EDIT HAK AKSES');
            $('#data-form-content').html(res);
        }
    })
}

function showDownloadSertificate() {
    $.ajax({
        url: `/certificate/index`,
        type: 'get',
        contentType: 'html',
        success: function (res) {
            $('#modal-app').modal('show');
            $('.modal-title').text('DOWNLOAD CERTIFICATE');
            $('#data-form-content').html(res);
        }
    })
}
