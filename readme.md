# OC-ExoApp V.1.0
>>>
This is an Exercise Project to make an online course using the LARAVEL framework. This project starts on March 27, 2020.

Author: Eko Sutrisno | Trainer : Saroyo Hendro Wibowo
>>>
---

## Konten dan Info

-   [Installation](#installation)
-   [Author](#author)
-   [License](#license)

---

## Installation
>>>

**Tip:** Pastikan komputer kamu sudah terinstall composer.
-   Download atau clone.
-   Kemudian buka terminal / cmd dan arahkan ke folder project yang sudah kamu download/clone.
-   jalankan perintah `composer install`
-   kemudian buat database dengan nama `onlinecourse` kemudian atur username dan password mysql kamu.
-   Buka file .env kemudian atur properti database kamu.
>>>

Setelah selesai semua, Lakukan migration untuk mengenerate semua tabel yang dibutuhkan

Gunakan perintah diterminal `php artisan migrate` untuk migration semua tabel **Info: Pastikan didalam folder project yang benar**

`composer dump-autoload` Untuk generate autoload sebelum menjalankan seeder

Jalankan seeder dengan perintah `php artisan migrate:fresh --seed` 

**Info: Akan mencreate Role & User dengan [Nama: Super Admin, Email: superadmin@exoapp.com, Psw: password].** yang nantinya dapat kamu gunakan untuk login sebagi Super Admin

Lalu jalankan server dengan perintah `php artisan serve`

## Author

**Eko Sutrisno**

## License

[MIT](https://choosealicense.com/licenses/mit/)
